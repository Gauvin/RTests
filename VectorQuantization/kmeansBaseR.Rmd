---
title: "kmeansBaseR"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

#Librairies
```{r}

library(tidyverse)
library(ggplot2)
library(magrittr)
library(glue)

set.seed(1)

```


# Data set generation
```{r}

#Generate points at x*scale with x \in {-1,1} for each of the numDim dimensions
#Each point on the numDim dimensional hypercube has equal probability 2**-numDim of being generated
#Then generate random gaussian noise arund these hypercube points
generateDataSet <- function(numObs=100, numDim=3,  scale=2, sdEps=0.5){
  
  df <- data.frame()
  for(i in 1:numObs){

    dfObs <- data.frame(  binaryCoeff = rbinom(numDim,1,0.5) )
    dfObs %<>% mutate( binaryCoeffTrans = ifelse( binaryCoeff> 0 , 1, -1) )
    dfObs %<>% mutate( x = binaryCoeffTrans * scale )
    dfObs %<>% mutate( x = x +  rnorm(numDim,sd = sdEps))
    
    class <- dfObs$binaryCoeff %*% map_dbl(1:numDim, ~2**(.x-1))
    df <- rbind(df,c(dfObs$x,class) )
  }
 
  colnames(df) <- c( map_chr(1:numDim, ~glue("x{.x}")), "class")
  
  return(df)
}

```

```{r}

df <- generateDataSet(numDim = 2,sdEps = 0.1)
df %>% head

ggplot(df) + 
  geom_point(aes(x=x1,y=x2,col=as.factor(class)))
```

# Generate K random centroids


```{r}



generateKRandomPoints <- function(df, k=5){
  
  stopifnot(nrow(df) >= k)
  df[sample(x = nrow(df), size = k,replace = F), ]
  
}

```


```{r}

dfCentroids <- generateKRandomPoints(df)
ggplot(dfCentroids) + 
  geom_point(aes(x=x1,y=x2,col=as.factor(class)))
```
 
#Compute the (Norm 1) between pairs of points
```{r}

computeNorm1Distance <- function(row1,row2){
  
  stopifnot(ncol(row1)== ncol(row2))
  d=0.0
  for (c in 1:length(row1)){
    d = d + abs(as.double(row1[[c]]-row2[[c]]))
  }
 return(d) 
}

```


```{r}

cols <- c("x1","x2")

sapply(1:nrow(dfCentroids),
       function(k) computeNorm1Distance(dfCentroids[k,cols],df[1,cols])
       )

dfCentroids[2, ]
df[1,]
```

#Get the nearest centroid
```{r}


getNearestCentroid <- function(dfCentroids, row, numDims){
  
  cols <- sapply(1:numDims, 
                 function(k) glue("x{k}"))
  
  distances <- sapply(1:nrow(dfCentroids),
                      function(k) computeNorm1Distance(dfCentroids[k,cols],row[cols]))

  dfCentroidsNearest <- dfCentroids[which.min(distances), ]
  dfCentroidsNearest$distance <- sort(distances)[[1]]
  dfCentroidsNearest$cluster <- which.min(distances)
    
  return(dfCentroidsNearest)
}
 

```


```{r}

getNearestCentroid(dfCentroids, df[1, ], numDims = 2)

```



#Recompute the new centroids
```{r}

recomputeCentroids <- function(df,numDim){
  
  cols <- sapply(1:numDim, 
                 function(k) paste("x", k, sep="" ))
  
  dfCentroids <- data.frame()
  for (cl in unique(df$cluster)){
    dfFiltered <- df[ df$cluster==cl, ]
    dfNewCentroid <- sapply(cols, function(k) mean(dfFiltered[[k]])) %>% t %>% data.frame()
    dfCentroids <- rbind(dfCentroids, dfNewCentroid)
  }
  
  return(dfCentroids)
  
}


```


```{r}

dfWithClust <- map_dfr( 1:nrow(df),
                        ~getNearestCentroid(dfCentroids, df[.x, ], numDims = 2) )

recomputeCentroids(dfWithClust, 2)

```

#K-means
```{r}

computeKMeans <- function(df,dfCentroids,numDim,maxIters=10){
  
  
  for(iter in 1:maxIters){
    
    
    dfWithClust <- map_dfr( 1:nrow(df),
                            ~getNearestCentroid(dfCentroids, df[.x, ], numDims = 2) )
    
    dfCentroid <- recomputeCentroids(dfWithClust, 2)
    
  }
  
  return(list(dfWithClust=dfWithClust,
              dfCentroid=dfCentroid))
  
}


```

```{r}

res <- computeKMeans(df,dfCentroids)


ggplot() + 
  geom_point(data=res$dfWithClust, aes(x=x1,y=x2,col=as.factor(cluster)))+ 
  geom_point(data=df, aes(x=x1,y=x2,col=as.factor(class))) + 
   geom_point(data=res$dfCentroid, aes(x=x1,y=x2,col=as.factor(class)) ) 
```