---
title: "Test spatial lite"
output: html_document
date: "2022-11-24"
---



```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Setup

## Libs

```{r}

library(RSQLite)

library(tidyverse)
library(magrittr)

library(here)
library(glue)

library(sf)

library(mapview)

library(ben.R.utils) #devtools::install_github("cgauvi/ben.R.utils")

```

  

  

---
---

# DB creation 

```{r}

dir_db <- here("data")
db_name <- file.path(dir_db,"test.db")
if(!dir.exists(dir_db)) dir.create(dir_db,recursive = T)

```


## Get some open data


```{r}

shp_qc_neigh <- st_read('https://www.donneesquebec.ca/recherche/dataset/5b1ae6f2-6719-46df-bd2f-e57a7034c917/resource/436c85aa-88d9-4e57-9095-b72b776a71a0/download/vdq-quartier.geojson')

st_write(shp_qc_neigh, here("data", 'shp_qc_neigh.shp'))

# Project to try diff projections
shp_mtl_neigh <- st_read('https://data.montreal.ca/dataset/c8f37ad6-16ff-4cdc-9e5a-e47898656fc9/resource/9f68405f-c658-42e1-a786-cb43a29e344e/download/quartiers_sociologiques_2014.geojson') %>% 
  st_transform(crs=32198)


```


```{r}

#https://gis.stackexchange.com/questions/167643/how-to-convert-a-complex-project-of-shapefiles-to-a-spatialite-database
# for the options
st_write(shp_mtl_neigh, 
         here("data",'shp_mtl_neigh.sqlite'), 
         driver = 'SQLite',
         dataset_options=c('SPATIALITE=YES'))

 
st_read(dsn= here("data",'shp_mtl_neigh.sqlite'), 
        query = 'SELECT st_area(geometry) as area from shp_mtl_neigh')

```


## Inspect tables and make sure we didn't overwrite any... 
```{r}


# Write table to new DB
write_table  (df = shp_qc_neigh,
                tbl_name = 'shp_qc_neigh',
                db_name = db_name,
                overwrite = T)

# Write table to new DB
write_table  (df = shp_mtl_neigh,
                tbl_name = 'shp_mtl_neigh',
                db_name = db_name,
                overwrite = T)

RSQLite::dbListTables(RSQLite::dbConnect(RSQLite::SQLite(), db_name)  )


```


```{r}
st_write(shp_qc_neigh,
                tbl_name = 'shp_qc_neigh',
                db_name = db_name,
                overwrite = T) )
```

```{r}
db = system.file("sqlite/meuse.sqlite", package = "sf")

RSQLite::dbListTables(RSQLite::dbConnect(RSQLite::SQLite(), db)  )

RSQLite::dbReadTable(RSQLite::dbConnect(RSQLite::SQLite(), db), "spatial_ref_sys")
st_read(dsn=db, layer='meuse.sqlite') %>% head

q_dist <- "SELECT ST_DISTANCE(ST_GeomFromText('POINT (181072 333611)', 28992), GEOMETRY) as dist FROM meuse.sqlite"
st_read(dsn=db, query = q_dist)

q_dist_2 <- "SELECT ST_DISTANCE(ST_GeomFromText('POINT (181072 333611)', 28992), SetSRID(GEOMETRY, 28992)) as dist from 'meuse.sqlite'"
st_read(dsn=db, query = q_dist_2)

```


---
---



```{r}
 sf::st_write(obj = shp_qc_neigh,
              layer = 'shp_qc_neigh',
              dsn =  here("data","new_test_add.db"),
              overwrite = T,
              spatialite = T,
              driver = 'SQLite')


dbExecute(RSQLite::dbConnect(RSQLite::SQLite(), here("data","new_test_add.db"),   loadable.extensions = TRUE), 
          "select AutoFDOStart();")
st_read(dsn =  here("data","new_test_add.db") ,
        query = "SELECT * from fdo_shp_qc_neigh'")
```

# Is spatial lite extension installed?

```{r}


dbExecute(RSQLite::dbConnect(RSQLite::SQLite(), db_name,   loadable.extensions = TRUE), "")
r <- dbExecute(RSQLite::dbConnect(RSQLite::SQLite(), db_name,   loadable.extensions = TRUE), " select spatialite_version(); ")

dbExecute(RSQLite::dbConnect(RSQLite::SQLite(), db_name,  loadable.extensions = TRUE), 
          "CREATE VIRTUAL TABLE 'fdo_shp_qc_neigh' USING VirtualFDO('shp_qc_neigh')")




shp_transformed <- st_read(dsn = db_name, 
                           query = "SELECT st_area(GEOMETRY) as area from 
                           (
                           CREATE VIRTUAL TABLE 'fdo_shp_qc_neigh' USING VirtualFDO('shp_qc_neigh') 
                           )
                           ")
shp_transformed <- st_read(dsn = db_name, 
                           query = "
                           CREATE VIRTUAL TABLE 'fdo_shp_qc_neigh' USING VirtualFDO('shp_qc_neigh') 
                           ")

rgdal::readOGR(dsn = db_name, driver = "SQLite")

assertthat::assert_that(!all(is.na(shp_transformed$area))) # didnt work

```
```{r}


db = system.file("sqlite/test3.sqlite", package = "sf")
dbcon <- dbConnect(dbDriver("SQLite"), db)
	  
dbGetQuery(dbcon, "SELECT st_transform(GEOMETRY,4326), ST_AREA(GEOMETRY) as area from Regions" )

st_read(dsn = db,   query = "SELECT st_transform(GEOMETRY,4326), ST_AREA(GEOMETRY) as area from Regions") %>% head
	  
```

```{r}

q <- "SELECT * \
        FROM TOWNS as t\
        INNER JOIN Regions as r\
        ON ST_WITHIN(t.geometry,r.geometry)\
"

st_read(dsn = db,   
        query = q
        ) %>% head
```




---
---


# CRS

## Inspect CRS
```{r}

# There are 2 different crs: one is geographic the other projected
ben.R.utils::get_df_tbl(db_name, "spatial_ref_sys")
ben.R.utils::get_df_tbl(db_name, "geometry_columns")

```


 

## Default CRS retrieval
```{r}

df_crs_spatial_lite <- ben.R.utils::get_df_tbl(db_name, "spatial_ref_sys")

# Reads in the crs correctly?
lapply(c('shp_mtl_neigh', 'shp_qc_neigh'),
      function(x) ({
        st_read(dsn = db_name, layer = x) %>% st_crs()
      })
)

```

## CRS transformations
```{r}

query_transformed <- glue::glue(
  "
  SELECT    st_transform(GEOMETRY,32198) as GEOMETRY
  FROM 'shp_qc_neigh'
  "
)

```

```{r}

# Doesn't seem to read in the new CRS automatically + desnt work 
shp_transformed <- st_read(dsn = db_name, 
                           query = query_transformed)
 

 
shp_transformed %>% head(10)
shp_transformed  
```


```{r}


shp_transformed_2 <- dbExecute(RSQLite::dbConnect(RSQLite::SQLite(), db_name), query_transformed)
shp_transformed_2

```
## Other spatial funs
```{r}

query_area <- glue::glue(
  "
  SELECT  area(geometry) as a, *
  FROM 'shp_qc_neigh';
  "
)

shp_area <- dbExecute(RSQLite::dbConnect(RSQLite::SQLite(), db_name,   loadable.extensions = TRUE), query_area)
shp_area


shp_transformed <- st_read(dsn = db_name, 
                           query = query_area)
shp_transformed

st_write(obj = shp_qc_neigh, dsn = here("data", "test_2.db"))

```



---
---



```{r}

db_sf_meuse <- system.file("sqlite/meuse.sqlite", package="sf")
st_read(system.file("sqlite/meuse.sqlite", package="sf"))

query_area <- glue::glue(
  "
  SELECT  area(geometry) as a, *
  FROM 'shp_qc_neigh';
  "
)
st_distance( 28992
```


---
---



## Spatial clauses (e.g. st_within)

```{r}

url_neigh <- "https://www.donneesquebec.ca/recherche/dataset/5b1ae6f2-6719-46df-bd2f-e57a7034c917/resource/436c85aa-88d9-4e57-9095-b72b776a71a0/download/vdq-quartier.geojson"

shp_neigh <- st_read(url_neigh)

# Faster to transform the polygon (with ~30 features) than the entire ~3*10**6 point dataset
geom_neigh <- shp_neigh %>% 
  st_transform(crs=4326) %>% 
  filter(NOM == "Montcalm") %>%
  pull(geometry) %>% 
  sf::st_as_text()

query_neigh <- glue::glue(
  "
  SELECT *
  FROM '{tbl_shp_name}'
  WHERE ST_WITHIN(GEOMETRY , st_geomfromtext('{geom_neigh}', 4326) )
  "
)

shp_policies_montcalm <- st_read(dsn = here("data","addresses.db"), query = query_neigh)
 

```

```{r}



```{r}

  sql_homes <- glue::glue("
      SELECT *
      FROM {tbl_shp_name} as t_left
      INNER JOIN
      (
        SELECT rl0105a , ID_PROVINC 
        FROM {tbl_dbf_name}
        WHERE rl0105a = 1000
      )as t_right
      ON t_left.'ID_PROVINC' = t_right'ID_PROVINC'
    ")


shp_results <- sf::st_read(dsn = conn, query = sql_homes)

RSQLite::dbGetQuery(conn, sql_homes)
```

---
---

# Spatial index creation 
```{r}

query_spatial_index <- "
  SELECT CreateSpatialIndex('rol_unite_p_2020.shp', 'GEOMETRY');
"

RSQLite::dbSendQuery()


```

---
---


<!-- ```{r} -->


<!-- db_other <-  "/sidecar/home/charles.gauvin/Dev/sf_test/../policies_map/data/addresses.db" -->



<!-- dbListTables(RSQLite::dbConnect(RSQLite::SQLite(), db_other) ) -->


<!-- df_temp <- dbReadTable(RSQLite::dbConnect(RSQLite::SQLite(), db_other), "tmp") -->

<!-- ben.R.utils::delete_tables(RSQLite::dbConnect(RSQLite::SQLite(), db_other), "tmp") -->
<!-- ``` -->


<!-- ``` -->


