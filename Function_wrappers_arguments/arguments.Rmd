---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(glue)
```


```{r}


ff <- function(y,x,a){
  print(glue('a: {a}, x:{x}, y:{y}'))
}

ff2 <- function(x,y,a){
  print(glue('a: {a}, x:{x}, y:{y}'))
}

ff3 <- function(b,...){
  calll <- match.call()
  print(calll)
  
  if (b<0){
    print('<0')
    ff(b,...)
    ff(a=b,...)
  }else{
    ff2(b,...)
  }
}


ff3(1, x=8, b=-1)
ff3(1, 't', a=-1 )

```



```{r}

test<-function(x,y,...)
{
 
  mc <- match.call(expand.dots = FALSE)
 
  print(glue('No expansion: x: {mc$x}'))
  
  mc3 <- match.call(expand.dots = T)
 
  print(glue('Expansion: x: {mc3$x}'))
}


test(1,2,a=9)
test(1,2,x=9)

```

