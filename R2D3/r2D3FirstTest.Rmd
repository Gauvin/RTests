---
title: "Untitled"
author: "Charles"
date: "November 27, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}

library(dplyr)
library(magrittr)

library(miceadds)
library(glue)
library(sf)

library(colorRamps)

library(leaflet)
 
library(neighShp)
 
library(r2d3)
```