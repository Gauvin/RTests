library(mgcv)
library(tidyverse)
library(magrittr)

set.seed(6)

## simulate some data from a three class model
n <- 1000
f1 <- function(x) sin(3*pi*x)*exp(-x)
f2 <- function(x) x^3
f3 <- function(x) .5*exp(-x^2)-.2
f4 <- function(x) 1
x1 <- runif(n);x2 <- runif(n)
eta1 <- 2*(f1(x1) + f2(x2))-.5
eta2 <- 2*(f3(x1) + f4(x2))-1
p <- exp(cbind(0,eta1,eta2))
p <- p/rowSums(p) ## prob. of each category 
cp <- t(apply(p,1,cumsum)) ## cumulative prob.
## simulate multinomial response with these probabilities
## see also ?rmultinom
y <- apply(cp,1,function(x) min(which(x>runif(1))))-1
## plot simulated data...
plot(x1,x2,col=y+3)

## now fit the model...
b <- gam(list(y~s(x1)+s(x2),~s(x1)+s(x2)),family=multinom(K=2))
summary(b) 
plot(b,pages = 1)

#Following fails with Error in qr.qty(qrx, y) : right-hand side should have 1000 not 2000 rows
# bp <- gam(list(y~s(x1)+s(x2),y~s(x1)+s(x2)),family=multinom(K=2))
# summary(bp) 

#Charles tests => question are 2 independent binomials equivalents to a single multinomial with 3 classes?
c <- gam(y[y < 2]~s(x1[y < 2])+s(x2[y < 2]),family=binomial)
summary(c) 
plot(c,pages = 1)

plot(b,pages=1)
gam.check(b)

## now a simple classification plot...
expand.grid(x1=seq(0,1,length=40),x2=seq(0,1,length=40)) -> gr
pp <- predict(b,newdata=gr,type="response")
pc <- apply(pp,1,function(x) which(max(x)==x)[1])-1
plot(gr,col=pc+3,pch=19)