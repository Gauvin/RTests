
require(miceadds)
require(mgcv)

miceadds::load.Rdata(filename= file.path( "dfWebPredGroupedBySession262137_dfBDFinal20181120.Rdata"),
                      objname = "dfWebPredGroupedBySession")

predictorString <- paste( "s(AGE)"	,
                          "s(TEL)" 	,
                          "s(FACE)" 	,
                          "s(DIGITAL)"	,
                          "s(totalPageLength)"	,
                          "s(ratioDistPagesOverVisits)"	, 
                          "numDistinctPages"	,               #don't use a spline on these integer variables => seems to be some numerical issues with number of knots
                          "numRepeatedPages"	,
                          "numDistinctPageCategs",
                          "categ_modification.1"	,           #don't use a spline on a binary variable
                          "errorPage",
                          "ARRET_POSTAL.O",
                          "ETAT_CIV.M",                                                  
                          "ETAT_CIV.V",                                                  
                          "ETAT_CIV.missing",
                          "SEXE.M",
                          collapse ="+",sep = "+")


formString1 <- as.formula(paste0(respStr , " ~ " , predictorString))
formString2 <- as.formula(paste0( " ~ " , predictorString))

#Test1 => with terms
listForm <- list(terms( formString1 , data = dfWebPredGroupedBySession[ idxTrain ,  ] ),
                 terms( formString2 , data = dfWebPredGroupedBySession[ idxTrain ,  ])
)

#Test2
listForm <- list(formString1,formString2)

gamFit <- mgcv::gam( formula= listForm,
                     family=multinom ,
                     data= dfWebPredGroupedBySession[ idxTrain ,  ] )