 
data {
  int <lower=1> N; //num individuals  
  int <lower=1> Nsim;
  int <lower=1> J; //num groups/subjects
  vector[N] Days; //covariate
  real <lower = 0> Reaction_time[N]; //response 
  
  int <lower=1, upper=J>observation_to_subject[N]; //observaion to subject/group mapping
  
  real sd_data;
 
}




parameters {

  real  mu_alpha; // random intercept model hyperpriors
  real <lower=0>  sigma_alpha; // random intercept model hyperpriors
  real alpha [J]; // random intercept (offset) 
  
  real beta; // common slope 
  real alpha_const_mean; //common intercept
  real  <lower=0>  sigma_resp; 
  
}

 
 
model {
  
  vector[N] alpha_subject;
  
  mu_alpha ~ normal(0, sd_data); //need to define distribution for hyperpriors in hierarchical model 
  sigma_alpha ~ cauchy(0,sd_data);
  alpha ~ normal(mu_alpha,  sigma_alpha);
  
  sigma_resp ~ cauchy(0,sd_data);
 
  beta ~ normal(0, 10);
  alpha_const_mean ~ normal(0, 10);
  
  for(n in 1:N)
    alpha_subject[n] = alpha[observation_to_subject[n]];
  
  
  for(n in 1:N)
    target += normal_lpdf (Reaction_time [n] | Days[n] * beta + alpha_const_mean + alpha_subject[n], sigma_resp) ;
}


//For posterior predictive validation
generated quantities {
  
 
  vector[N] gen_reaction_time_v2;
  
  for(n in 1:N){
    gen_reaction_time_v2[n] = normal_rng( Days[n]  * beta   +  alpha_const_mean +  alpha[observation_to_subject[n]]  , sigma_resp );
  }
  
 
  
}
