---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```



# Setup
 
## Libs
```{r}

library(tidycensus)
library(tidyverse)
library(magrittr)
library(tigris)
library(here)
library(sf)
library(snapbox)
library(glue)


library(tictoc)

library(lme4)
library(bayesplot)
library(rstan)
library(tidybayes)

```
  

## Theme
```{r}
custom_minimal_theme <- function(){  ggplot2::theme_minimal(base_family="Roboto Condensed", base_size=11.5) }
```
 
---
---

# EDA 
```{r}

ggplot(sleepstudy) + 
  geom_point(aes(x=Days,y=Reaction)) + 
  facet_wrap(~Subject)

ggplot(sleepstudy) + 
  geom_point(aes(x=Days,y=Reaction,col=Subject)) 

```


---
---



# Lmer

```{r}

 
lmer(Reaction ~ Days + (1 | Subject), sleepstudy) %>% summary()


```

```{r}


lm(Reaction ~ Days , data=sleepstudy) %>% summary()

```


---
---

# Stan


## Data input

```{r}

sleepstudy %<>% mutate(observation_to_subject = Subject %>%  as.integer()) #needs to be an integer, not a factor

stan_data <- map(0:6,
  ~list( N=nrow(sleepstudy),
        Nsim=10,
        J=n_distinct(sleepstudy$observation_to_subject ),  #1..num_sujects 
        Reaction_time = sleepstudy$Reaction,
        observation_to_subject=sleepstudy$observation_to_subject,  
        Days = sleepstudy$Days ,
        sd_data=10**.x
  )
)

```

---
---


## Stan fit



### Benchmark brms
```{r,results=hide}

brms_fit <- brms::brm( Reaction ~ 1+ (1 + Days | Subject),  data=sleepstudy,
             chains = 4,             # number of Markov chains
           warmup = 50,          # number of warmup iterations per chain
           iter = 250,            # total number of iterations per chain
           cores = 4,              # number of cores (could use one per chain) - dont think it is possible to use more than 1 thread per chain
           refresh = 0    )

```



### Sanity check - first model with more concentrated priors 
```{r,results=hide}

tic()

fit_stan_concentrated <- stan(
  file = "stan_sleep_random_intercept_slope.stan",  # Stan program
  data = stan_data[[2]],    # named list of data
  chains = 4,             # number of Markov chains
  warmup = 50,          # number of warmup iterations per chain
  iter = 250,            # total number of iterations per chain
  cores = 4,              # number of cores (could use one per chain) - dont think it is possible to use more than 1 thread per chain
  refresh = 0            # no progress shown
  )


toc()


summary(fit_stan_concentrated)[[1]] %>% as.data.frame()


```




### Sanity check - compare with diffuse priors 
```{r}

tic()

fit_stan_diffuse <- stan(
  file = "stan_sleep_random_intercept_slope.stan",  # Stan program
  data = stan_data[[6]],    # named list of data
  chains = 4,             # number of Markov chains
  warmup = 50,          # number of warmup iterations per chain
  iter = 250,            # total number of iterations per chain
  cores = 4,              # number of cores (could use one per chain) - dont think it is possible to use more than 1 thread per chain
  refresh = 0            # no progress shown
  )


toc()


summary(fit_stan_diffuse)[[1]] %>% as.data.frame() #first chain results 


```


---
---


## Parameter/fit inspection


```{r}
num_subject <- n_distinct(sleepstudy$Subject) 

alpha_str <- paste0('alpha[', 1:num_subject , ']')
beta_str <- paste0('beta[', 1:num_subject , ']')

all_pars <- c(beta_str, 
             alpha_str,
             "alpha_const_mean",
             "sigma_resp",
             "mu_alpha", "sigma_alpha")

```

### Brms
```{r}

print(brms_fit, 
      pars=c(all_pars , 
             "lp__"),  #log likelihood
      probs=c(.1,.5,.9))

```


### Concentrated
```{r}

print(fit_stan_concentrated, 
      pars=c(all_pars , 
             "lp__"),  #log likelihood
      probs=c(.1,.5,.9))

```

### Diffuse
```{r}

print(fit_stan_diffuse, 
      pars=c(all_pars , 
             "lp__"),  #log likelihood
      probs=c(.1,.5,.9))

```


---
---

## Traceplots 


### BRMS
```{r}

plot(brms_fit)

brms_fit$fit

```


### Concentrated
```{r}
 
posterior <- extract(fit_stan_concentrated, inc_warmup = TRUE, permuted = FALSE)

  
alpha_first_from_subj <-  paste0( 'alpha[', 1:10 ,']')
beta_first_from_subj <-  paste0( 'beta[', 1:10 ,']')

pars_alpha_first_from_subj  <- c(beta_first_from_subj[1:4], 
             alpha_first_from_subj [1:4], 
             "sigma_resp",
             "mu_alpha", "sigma_alpha")

p <- mcmc_trace(posterior,  pars = pars_alpha_first_from_subj, n_warmup = 300,
                facet_args = list(nrow = 2, labeller = label_parsed))
p + facet_text(size = 15)

```
### Make sure the alphas for the same subject are the same 
```{r}
 
posterior <- extract(fit_stan_concentrated, inc_warmup = TRUE, permuted = FALSE)



p <- mcmc_trace(posterior,  pars = pars_alpha_first_from_subj, n_warmup = 300,
                facet_args = list(nrow = 2, labeller = label_parsed))
p + facet_text(size = 15)

```




### Diffuse
```{r}
 
posterior <- extract(fit_stan_diffuse, inc_warmup = TRUE, permuted = FALSE)



p <- mcmc_trace(posterior,  pars = pars_alpha_first_from_subj, n_warmup = 300,
                facet_args = list(nrow = 2, labeller = label_parsed))
p + facet_text(size = 15)

```


---
---

## Posterior distribution
```{r}

 
mcmc_areas(as.matrix(fit_stan_concentrated),
           pars =all_pars,
           prob = 0.8) + 
  ggtitle('Posterior distributions')


```

---
---

## Posterior predictive distribution 

```{r}

stan_mod <- stan_model( file = "stan_sleep_random_intercept_slope.stan")
```


#### Concentrated priors

##### More iterations
```{r}

f <- sampling(stan_mod, data=stan_data[[2]],  iter = 2000, warmup =100,refresh=0)
y_pred <- as.matrix(f, pars = "gen_reaction_time_v2")

 
bayesplot::ppc_dens_overlay(y =sleepstudy$Reaction , 
                            yrep = y_pred) + ggtitle('2000 iterations and 100 burn-in')
 
```

##### Less iterations
```{r} 
f2 <- sampling(stan_mod, data=stan_data[[2]],  iter = 250, warmup =50,refresh=0)
y_pred2 <- as.matrix(f2, pars = "gen_reaction_time_v2")

 
bayesplot::ppc_dens_overlay(y =sleepstudy$Reaction , 
                            yrep = y_pred2) + ggtitle('250 iterations and 50 burn-in')
 
                     
```


#### Diffuse prior

```{r}

f <- sampling(stan_mod, data=stan_data[[4]], iter = 50,refresh=0)

y_pred <- as.matrix(f, pars = "gen_reaction_time_v2")

 

bayesplot::ppc_dens_overlay(y =sleepstudy$Reaction , 
                            yrep = y_pred) 
```


---
---


```{r}


df_pred <- data.frame(pred=predict(brms_fit) %>% as.data.frame() %>% pull(Estimate) ,
                      resp=sleepstudy$Reaction) %>% mutate(obs=1:nrow(sleepstudy))

df_pred_long <- df_pred %>% pivot_longer(-obs)

ggplot(df_pred_long ) + 
  geom_density(aes(x=value,col=name))
  
ggplot(df_pred) + 
  geom_point(aes(x=pred,y=resp))

mean_resp <- mean(df_pred$resp)
Rsq <-sum((df_pred$pred-mean_resp)**2)/sum((df_pred$resp-mean_resp)**2)
Rsq

```

