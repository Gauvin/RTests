 
data {
  int <lower=1> N; //num individuals  
  vector[N] Days; //covariate
  real <lower = 0> Reaction_time[N]; //response 
  real lb;
  real ub;
}




parameters {

  real alpha ;  
  real beta;
  real  <lower=0>  sigma_resp; 
  
}

 
 
model {
  
  beta ~ uniform(lb, ub);
  alpha ~ uniform(lb, ub);
  sigma_resp ~ cauchy(0,10);
  
  for(n in 1:N)
    target += normal_lpdf (Reaction_time [n] | Days[n] * beta + alpha, sigma_resp) ;
}

 
