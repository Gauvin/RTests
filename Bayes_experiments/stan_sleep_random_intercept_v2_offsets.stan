 
data {
  int <lower=1> N; //num individuals  
  int <lower=1> Nsim;
  int <lower=1> J; //num groups/subjects
  vector[N] Days; //covariate
  real <lower = 0> Reaction_time[N]; //response 
  
  int <lower=1, upper=J>observation_to_subject[N]; //observaion to subject/group mapping
}




parameters {

  real  mu_alpha; // random slope model hyperpriors
  real <lower=0>  sigma_alpha; // random slope model hyperpriors
  
 
  vector [J] alpha_offset;
  
  real beta;
  real  <lower=0>  sigma_resp; 
  
}

 
 
model {
  
  vector[J] alpha_observation;
  
  mu_alpha ~ normal (0,10);
  sigma_alpha ~cauchy(0,10);
  alpha_offset ~ normal(0,1);
  
  beta ~ normal(0, 10);
 
  for(n in 1:N)
    alpha_observation[n] =  mu_alpha + sigma_alpha* alpha_offset[ observation_to_subject[n] ];
  
  
  for(n in 1:N)
    target += normal_lpdf (Reaction_time [n] | Days[n] * beta + alpha_observation[n], sigma_resp) ;
}


//For posterior predictive validation
generated quantities {
  
  matrix[Nsim, J] gen_reaction_time;


  for(n in 1:Nsim) {
    for(j in 1:J){
         gen_reaction_time[n,j] = normal_rng( Days[n] * beta  +     mu_alpha + sigma_alpha* alpha_offset[j] , sigma_resp );
    }
 

  }
  
  
}
