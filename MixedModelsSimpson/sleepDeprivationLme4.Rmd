---
title: "Untitled"
author: "Charles"
date: "15/11/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

#Librairies
```{r}
 
 
library("lme4") 
library("magrittr")
library("dplyr")
library(tidyverse) 
```


```{r}

sleepstudy %>% str


sleepstudy %>% count(Subject)

sleepstudy %>% summary

sleepstudy %>% head
```

```{r}

ggplot(sleepstudy, aes(x=Days, y=Reaction)) + 
  geom_line() + 
  geom_smooth(method="lm") + 
  facet_wrap(~Subject)

```
```{r}

ggplot(sleepstudy,) + 
  geom_line(aes(x=Days, y=Reaction,col=Subject))  +
  geom_smooth(aes(x=Days, y=Reaction), method="lm")

```

```{r}

lmFit <- lm( Reaction ~  Days , data = sleepstudy )

summary(lmFit)

```



```{r}

lmer( Reaction ~ Days + (1|Subject) , data = sleepstudy )

```

#Random slope for Days and intercept + can be correlated 
```{r}

mixedlmFit <- lmer( Reaction ~ Days + (Days|Subject) , data = sleepstudy )

ranef(mixedlmFit)

```

#Random slope and intercept + UNcorrelated
```{r}

mixedlmFitNoCorr <- lmer( Reaction ~ Days + (Days||Subject) , data = sleepstudy )

mixedlmFitNoCorr %>% summary

ranef(mixedlmFitNoCorr)
```