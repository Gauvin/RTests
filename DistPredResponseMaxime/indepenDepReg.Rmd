---
title: ""
author: "Charles"
date: "March 27, 2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


#Introduction

We are investigating the following question: can a covariate be non-significant in a univariate model, but significant when adding a new covarite. 

$Y= X_1 \beta_1 + X_2 \beta_2 + \epsilon$ where $ \beta_2 X_2= Y- X_1 \beta_1$, but $E[X_2 Y] =0$ and we assume all rv have zero mean

#Libs
```{r}

library(tidyverse)
library(magrittr)

library(ggplot2)
library(patchwork)
library(ggpubr)
library(ggExtra)
library(cowplot)
```

#Parameters
```{r}

numObs <- 1000
beta1 <- 5
beta2 <- 2
 
 
 
```

#Generate data
```{r}
 
  df <- data.frame(covar1=rnorm(n=numObs, mean=0,sd=varCovar**0.5))
  df %<>% mutate(obs = beta1*covar1  +  rnorm(n=numObs, mean=0,sd=1))
  df %<>% mutate(xminusy= (beta1*covar1-obs)   )
  df %<>% mutate(covar2= xminusy/beta2 + rnorm(n=numObs, mean=0,sd=0.1))
 
  
  
  df %>% head
  
```

#Data inspection

## Y vs X1
```{r}


# Scatter plot colored by covars ("Species")
sp <- ggscatter(df, x = "covar1", y = "obs",
                palette = "jco",
                size = 3, alpha = 0.6)+
  border()                                         
# Marginal density plot of x (top panel) and y (right panel)
xplot <- ggdensity(df, "covar1",
                   palette = "jco",
                   rug=T)
yplot <- ggdensity(df, "obs", 
                   palette = "jco",
                   rug=T)+
  rotate()

# Cleaning the plots
sp <- sp + rremove("legend")
yplot <- yplot + clean_theme() + rremove("legend")
xplot <- xplot + clean_theme() + rremove("legend")

# Arranging the plot using cowplot

gridPlotObsCovar <- plot_grid(xplot, NULL, sp, yplot, 
          ncol = 2, 
          align = "hv", 
          rel_widths = c(2, 1), 
          rel_heights = c(1, 2))


(gridPlotObsCovar)

```


## Y vs X2
```{r}


# Scatter plot colored by covars ("Species")
sp <- ggscatter(df, x = "covar2", y = "obs",
                palette = "jco",
                size = 3, alpha = 0.6)+
  border()                                         
# Marginal density plot of x (top panel) and y (right panel)
xplot <- ggdensity(df, "covar2",
                   palette = "jco",
                   rug=T)
yplot <- ggdensity(df, "obs", 
                   palette = "jco",
                   rug=T)+
  rotate()

# Cleaning the plots
sp <- sp + rremove("legend")
yplot <- yplot + clean_theme() + rremove("legend")
xplot <- xplot + clean_theme() + rremove("legend")

# Arranging the plot using cowplot

gridPlotObsCovar <- plot_grid(xplot, NULL, sp, yplot, 
          ncol = 2, 
          align = "hv", 
          rel_widths = c(2, 1), 
          rel_heights = c(1, 2))


(gridPlotObsCovar)

```

## X2 vs error
```{r}


# Scatter plot colored by covars ("Species")
sp <- ggscatter(df, x = "xminusy", y = "covar2",
                palette = "jco",
                size = 3, alpha = 0.6)+
  border()                                         
# Marginal density plot of x (top panel) and y (right panel)
xplot <- ggdensity(df, "xminusy",
                   palette = "jco",
                   rug=T)
yplot <- ggdensity(df, "covar2", 
                   palette = "jco",
                   rug=T)+
  rotate()

# Cleaning the plots
sp <- sp + rremove("legend")
yplot <- yplot + clean_theme() + rremove("legend")
xplot <- xplot + clean_theme() + rremove("legend")

# Arranging the plot using cowplot

gridPlotObsCovar <- plot_grid(xplot, NULL, sp, yplot, 
          ncol = 2, 
          align = "hv", 
          rel_widths = c(2, 1), 
          rel_heights = c(1, 2))


(gridPlotObsCovar)

```
 

```{r}

glm(obs~covar1+covar2, data=df) %>% summary

```

```{r}

glm( covar1 ~ covar2, data=df) %>% summary

```

  