---
title: "Untitled"
author: "Charles"
date: "March 27, 2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

#Libs
```{r}

library(tidyverse)
library(magrittr)
library(ggplot2)
library(patchwork)
```

#Parameters
```{r}

numObs <- 100
diffMeans <- 10
ratioInterIntra <- 5
stdDev <- diffMeans/ratioInterIntra
```

#Generate data
```{r}

#Small variation around group means, but large differences across different groups

df <- data.frame(group=sample(c("g1","g2"),size = numObs, prob = c(0.4,0.6), replace = T) %>% as.factor(),
                 res = rnorm(n=numObs, mean=0,sd=stdDev))
df %<>% mutate( groupInt = as.integer(group)-1)

df %<>% mutate( obs = groupInt*diffMeans + res)

df %>% head
```


#Inspect

## Reponse
```{r}

pObsAll <- ggplot(df,aes(x=obs)) + 
  geom_density() + 
  geom_rug()

pObsByGroup <- ggplot(df, aes(y=group, x=obs)) + 
  ggridges::geom_density_ridges()  +
  geom_rug()

(pObsByGroup)

(pObsAll)

```



## Fit a glm + check dist of pred
```{r}
glmFit <- glm(obs ~ group, data= df)
glmFit %>% summary

dfPred <- glmFit %>% broom::tidy()

df$pred <- predict(glmFit)

ggplot(df) + 
  geom_boxplot(aes(x=group, y=pred))

ggplot(df, aes(group, obs)) +
  ggpmisc::stat_fit_tb(label.x  = "right", label.y = "bottom" )+
    stat_summary(fun.data = "mean_se") 


```

## Now the residuals
```{r}


df$resid <- resid(glmFit)

pBox <- ggplot(df) + 
  geom_boxplot(aes(x=group, y=resid)) + 
  coord_flip()+ 
  scale_y_continuous(limits = c(-5, 8))


pRidge <- ggplot(df) + 
  ggridges::geom_density_ridges(aes(y=group, x=resid)) + 
  scale_x_continuous(limits = c(-5, 8))
 
pResidAll <-  ggplot(df,aes( x=resid)) + 
  geom_density()  +
  geom_rug()

(pResidAll)

pBox/ pRidge

```

## All 3
```{r}

pObsAll <- ggplot(df,aes(x=obs)) + 
  geom_density() + 
  geom_rug() + 
  scale_x_continuous(limits = c(-15, 20))

pPredAll <- ggplot(df %>% group_by(pred) %>% summarise(num=n() ) %>% mutate(density = num/sum(num)), aes(x=pred,y=density)) + 
  geom_col() + 
  scale_x_continuous(limits = c(-15, 20))

pResidAll <-  ggplot(df,aes( x=resid)) + 
  geom_density()  +
  geom_rug() + 
  scale_x_continuous(limits = c(-15, 20))


 
pPatch <- (pObsAll + pPredAll) / (pResidAll + plot_spacer())

ggsave(filename = here::here("Figures","distributionsRespVsPredVsResidCategorical.png") ,
       plot=pPatch,
       width = 10,
       height = 10)

```