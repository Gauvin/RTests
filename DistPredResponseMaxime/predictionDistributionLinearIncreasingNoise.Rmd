---
title: "Distribution of conditional expectation and response"
author: "Charles"
date: "March 27, 2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


#Introduction

Consider the linear relationship $Y=X\beta + \epsilon$ where 

* $E[X]=\mu$, 
* $E[\epsilon|X]=E[\epsilon]=0$ and 
* $E[\epsilon^2]=\sigma^2$, 
* $E[Y|X]=X\beta$. 

The second bullet indicates that the random component and the prediction are uncorrelated: $E[\epsilon E[Y|X] ]  =0$. 

Assume we know this relationship perfectly, and $\beta$ need not be estimated from data. We choose to predict $E[Y|X]=X\beta$ as this is the $X$-measurable estimator $f$ that minimizes $E[ (Y-f)^2]$. In that case, the variance of the response will be much larger than the one of the prediction if and only if the variance of the random component is large. Indeed,we get:

$$
\begin{align}
\text{Var}(Y) &= E[ (E[Y|X]+\epsilon - E[Y] )^2 ] \\
&=  E[ E[Y|X]^2- 2 \epsilon \mu - 2 E[Y|X] \mu + 2 E[Y|X] \epsilon + \epsilon^2  + \mu ^2 ] \\
&= E[ E[Y|X]^2] ]  - \mu ^2 + \sigma^2 \\
& = \text{Var}( E[Y|X]) + \sigma^2
\end{align}
$$

 
since $\text{Var}( E[Y|X] ) = E[(E[Y|X] - \mu)^2] = E[(E[Y|X]^2 - 2E[Y|X]\mu + \mu^2] = E[(E[Y|X]^2 -  \mu^2]$

#Libs
```{r}

library(tidyverse)
library(magrittr)

library(ggplot2)
library(patchwork)
library(ggpubr)
library(ggExtra)
library(cowplot)
```

#Parameters
```{r}

numObs <- 100
fact <- 10
 

shp <- 2
sca <- 2
varCovar <- shp*sca**2

```

#Generate data
```{r}


generateDfNoise <- function(ratioVar){
  
  varResp <- varCovar*ratioVar*fact**2
  
  #Small variation around covar means, but large differences across different covars
  df <- data.frame(covar=rgamma(n=numObs,shape=shp,scale=sca),
                   rndErrTerm = rnorm(n=numObs, mean=0,sd=varResp**0.5))
   
  df %<>% mutate(varRespId = varResp)
  
  df %<>% mutate( obs = fact*covar + rndErrTerm)
}

#map_df with row_bind the df
listDf <- map( c(0.5,2.5,10.5), generateDfNoise)

listDf[[1]] %>% head
```

 

# Fit a glm + check dist of pred
```{r}

fitGlm <- function(df){
  glmFit <- glm(obs ~ covar, data= df)
  glmFit %>% summary

  df$pred <- predict(glmFit)
  df$resid <- resid(glmFit)

  return(df)
}

listFittedGlm <- map( listDf, fitGlm)


```

# Concat all df together
```{r}

dfConcat <- listFittedGlm %>% plyr::ldply(.id="varRespId")

dfMelted <- dfConcat %>%
  dplyr::select(  obs  ,   pred ,varRespId) %>% 
  reshape2::melt(id.vars=c("varRespId"))

dfConcat %>% dim

```
 

## All 3
```{r}
#  
# 
# pObsPredGivenVar <- ggplot(dfMelted,aes(x=value,fill=variable)) + 
#   geom_density(alpha=0.2) + 
#   geom_rug( ) + 
#   facet_wrap(~varRespId) +
#   labs(caption = "Response and prediction distribution\naccording to the variance of the random error term eps in \n y=beta x + eps")
# 


pObsPredGivenVarV2 <- ggplot(dfConcat) + 
  geom_density(aes(x=obs), fill="red", alpha=0.2) + 
  geom_density(aes(x=pred), fill="blue", alpha=0.2) + 
  #
  geom_rug(aes(x=obs), fill="red", alpha=0.2) + 
  geom_rug(aes(x=pred), fill="blue", alpha=0.2) + 
  #
  ggpmisc::stat_fit_tb(aes(x=covar,y=obs), label.x  = "right", label.y = "top" )+
  #
  facet_wrap(~varRespId) +
  #
  labs(caption = "Response and prediction distribution\naccording to the variance of the random error term eps in \n y= 10 x + eps\nResponse in red, prediction in blue") + 
  ggtitle("Effect of increasing noise in prediction and response distributions")
 

ggsave(filename = here::here("Figures","distributionsRespVsPred3Vars.png") ,
       plot=pObsPredGivenVarV2,
       width = 20,
       height = 10)


```