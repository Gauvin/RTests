library(stats)
library(magrittr)
library(tidyverse)
library(broom)

#y = X1+X2+e, e ~ N(0,100)

numVars <- 1000
vectCounter <- rep(0,numVars)
numSim <- 10

for(k in 1:numSim){
  nObs <- 2000
  df <- data.frame(x1=rnorm(nObs,10,5),     #mean of 10, var of  5**2
                   x2=rgamma(nObs, 100/10)  #mean of 100/10 = 10 + var of 100**2/10 = 10**3
  )
  df %<>% mutate(y=x1+x2+rnorm(nObs,0,10) ) #mean of 0, var of  10**2
  
  #Generate 4 extra random vars
  #Xi=2 = X1+X2+k k~N(i,i*100) i=1,...,2
  numCovaLinDep <- numVars-2
  for(i in 1:numCovaLinDep){
    noise <- rnorm(nObs,i,i*100)
    varName <- paste0("x",(i+2))
    df[[varName]] <- noise
  }
  
 
  
  
  #don't add the x1 var in lm
  dfNox1 <- df  
  lmFit <- lm( "y~." , data=dfNox1)
  #summary(lmFit)
  
  lm_model_coefficients <- tidy(lmFit)
  

  vectCounter <- vectCounter + as.integer(lm_model_coefficients$p.value < 0.05)
}

plot(vectCounter)


table(vectCounter)
#with n=5000
# 0   1   2     3   10
# 600 322  69   8   2 

table(vectCounter)
#with n=2000
# 0   1   2     3  10 
# 586 322  80  11   2 