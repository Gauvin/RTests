library(stats)
library(magrittr)
library(tidyverse)
library(broom)

#y = X1+X2+e, e ~ N(0,100)

vectCounter <- rep(0,4)
numSim <- 10

for(k in 1:numSim){
  nObs <- 5000
  df <- data.frame(x1=rnorm(nObs,10,5),     #mean of 10, var of  5**2
                   x2=rgamma(nObs, 100/10), #mean of 100/10 = 10 + var of 100**2/10 = 10**3
                   e=rnorm(nObs,0,10)       #mean of 0, var of  10**2
  )
  df %<>% mutate(y=x1+x2+e)
  
  #Generate 4 extra random vars
  #Xi=2 = X1+X2+k k~N(i,i*100) i=1,...,2
  numCovaLinDep <- 2
  for(i in 1:numCovaLinDep){
    noise <- rnorm(nObs,i,i*100)
    varName <- paste0("x",(i+2))
    df[[varName]] <- df$x1 + df$x2 + noise
    
  }
  
  #df$x5 <- purrr::map_dbl( 1:nrow(df) , ~ifelse(df$e[[.x]]>20,0,df$e[[.x]]+rnorm(1,0,2) ) )
  
  # #Check out the relationships
  # pairs( as.formula(" y~  ." ), data=df ,  lower.panel=NULL)
  # 
  # 
  # dfMelted <- df %>% 
  #   dplyr::select(x1,x2) %>% 
  #   gather(key="var")
  # 
  # ggplot( dfMelted ) + geom_density(aes(x=value,color=var))
  
  #Generate another 
  
  
  #don't add the x1 var in lm
  dfNox1 <- df %>% dplyr::select(-one_of("x1","e"))
  lmFit <- lm( "y~." , data=dfNox1)
  summary(lmFit)
  
  lm_model_coefficients <- tidy(lmFit)
  
  
  
  vectCounter <- vectCounter + as.integer(lm_model_coefficients$p.value < 0.05)
}

