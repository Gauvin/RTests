#! /bin/bash

#Author: Charles Gauvin
#April 1st 2022
#Code used to download the data-geographies-prod-list lambda (too large to inspect in the inline code editor)

#First set some profile with the correct region and credentials in ~/.aws/credentials

#Next set the desired profile used with
export AWS_PROFILE=charles_gauvin_east_1

#Check profile and make sure it is correctly set up to point at charles_gauvin_east_1
aws configure list

# Download the source code as a zip file 
aws lambda get-function --function-name arn:aws:lambda:us-east-1:894178623983:function:data-geographies-prod-list --query 'Code.Location' | xargs curl -o data-geographies-prod-list.zip

# Unzip to a new directory
unzip data-geographies-prod-list.zip -d data-geographies-prod-list
