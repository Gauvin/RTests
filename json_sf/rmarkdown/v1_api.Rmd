---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```



# Setup
## Librairies 
```{r}
options("rgdal_show_exportToProj4_warnings"="none")
library(sf)
library(here)
library(tidyverse)
library(magrittr)
library(glue)

library(tidycensus)
library(cancensus)
library(snapbox)

library(glue)

library(geojsonsf)
library(jsonlite)
library(mapview)

library(httr)
```


# Utils
```{r}

# for( f in list.files(here('R'), full.names = T, pattern = '.R$')){
#   source(f)
# }

# path_R_utiles <- '/home/charles/Documents/Dev/GIS/Python/insights/utilities/R/'
# for( f in list.files(path_R_utiles, full.names = T,recursive = T, pattern = '.R$')){
#   source(f)
# }

#Sys.getenv("INSIGHTS_PATH")  must be set in ~/.Renviron (R file with environment variables - the bash env vars are not loaded by default)
source( file.path( Sys.getenv("INSIGHTS_PATH") , "utilities","R", "local_logic" , "api_v1_helpers.R"     ) )
```



---
---

# Inspect LL api call directy

## V1
```{r}

# Need to read in as a json (NOT a geojson) - the geometry is encoded 


#geojsonsf::geojson_sf(api_call)

shp_poly_to <- get_polygons_from_boundaries_enpoint(lat= -79.41141907450654 , lng=43.682685505724315 )

mapview(shp_poly_to)
```


```{r}

shp_poly_qc <- get_polygons_from_boundaries_enpoint( )

```

