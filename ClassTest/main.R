
rm(list= ls() )
setwd(normalizePath(  "Y:\\RProjects\\ClassTest"  ) )

library(R6)

source("./Parent.R")
source("./Child.R")




myChild <- Child$new("run some tests")

print( paste0( "Results of myChild$implementation() ",  myChild$implementation() ) )
print( paste0( "Results of myChild$getParentAttribute() ",  myChild$getParentAttribute() ) )


myChild$setAttribute( "mod string ")

print( paste0( "Results of myChild$implementation() ",  myChild$implementation() ) )
print( paste0( "Results of myChild$getParentAttribute() ",  myChild$getParentAttribute() ) )
