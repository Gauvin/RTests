


Child <- R6Class(
  
  #
  #Name
  #
  
  "Child",
  
  
  #
  #Inheritance
  #
  
  inherit = Parent,
  
  
  #---
  #Public
  #---
  
  public = list(
    
    
    
    #' Constructor
    #'
    
    initialize=function(myStr){
      super$initialize(myStr)
      private$attribute <- "dummy"
    },
    
    
    
    getAttribute = function(){return( private$attribute )} ,      
    
    getParentAttribute =  function(){return( super$getAttribute() )} ,       
    
    implementation = function() {return (super$defaultImplementation() )},
    
    
    setAttribute = function ( newStr ) { private$attribute  <- newStr }
    
  ),
  
  
  private = list(
    attribute = NULL
    
  )
  
   
  
  
  
)
