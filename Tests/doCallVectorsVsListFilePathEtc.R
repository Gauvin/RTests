
library("magrittr")
library("dplyr")
library("tidyr")
library("fs")
library("purrr")


############# get the path + clean "" and NAs ###########


path <- '/home/dhw3172/projets/AgencyFootprintPIV2Rpackage/data/Rdata' #for tests with the linux machine: 
#path <- getwd() #for tests on the windows machine


#Don't remove the ""  <--- this seems correct for both unix and windows
listDirs <- strsplit( path, .Platform$file.sep) %>% unlist  # returns a chr vector; without the unlist, we get a list of 1 
listDirsFiltered <- listDirs


#Remove the "" <-- for sure results in a en rror for unix since /home/user/ gets mapped to home/user/ which does not exist
#listDirs[listDirs==""] <- NA
#listDirsFiltered <- na.omit(listDirs) #only works on a vector + creates some weird attributes



listDirsFiltered %<>% as.list() #creates a list with same size as listDirs - number of NAs


# listDirsFiltered <- listDirs %>% 
#   as.data.frame() %>% 
#   tidyr::drop_na() %>% 
#   dplyr::pull(.) %>% 
#   as.character() %>% 
#   list()#bit messy since we convert from df and back to list



############# failure on path creation ###########

#This fails
#This works on fs::path(...) but ... should not be a list
fs::path(listDirsFiltered)
fs::path(listDirsFiltered[2:4])
#listDirs <- na.omit(listDirs) # this creates a new list without NA,s but with weird attributes


############# sucess on path creation ###########

#This works
do.call(file.path,listDirsFiltered)
#This also works
do.call(fs::path, listDirsFiltered)


############# fct to get subpaths for different depths ###########


getSubPath <- function(listDirs, maxDepth){
  
  listDirsSubPath <- listDirs[1:maxDepth]
  subPath <- do.call(file.path, listDirsSubPath)
  return(subPath)
}

#With the leading "" go to depth >= 2 <-- empty dir does not exist for sure
for(depth in 1:length(listDirsFiltered)){
  subDirPath <- getSubPath(listDirsFiltered,depth)
  print(paste0( depth , ": " ,  subDirPath, " -- dir exists? : " ,  dir.exists(subDirPath) ) )
  
}




#############generic test on do.call ###########

#Using unamed list works if f accepts ... 
#Othersie, strick with named list list(args1=bla,arg2=foo)
someTest <- function(arg1,arg2){
  
  print(paste0(arg1, arg2))
}

someTest(list("a","b"))
do.call(someTest, list("a","b") )
