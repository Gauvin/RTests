---
title: "bias variance tradeoff"
author: "Charles"
date: "December 2, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

library(magrittr)
library(tidyverse)

library(here)
 
library(reshape2)

library(glue)
 
library(glmnet)


set.seed(1)

```


#Generate a dataset: convex quadratic with some noise
 
 
```{r}

#Generate some random data 
#y = f(x) + epsilon where f(x) = ax^2 +bx +c and a < 0 (convex quadratic)

a <- runif(1,7,10)
b <- -a*5*2  #x* > 0 <-> x*=-b/2a > 0  <-> b > 0 if a < 0 #fix the max at x*=5
c <- runif(1,-10,10)


x <- seq(-2,15,by=0.1)
yMean <- a*x**2+b*x+c

eps <- rnorm(length(yRaw),mean = 0,sd=50)

y <- yMean+eps

dfDat <- data.frame(x=x,y=y,yMean=yMean)
dfDat$const <- 1

dfDat %>% head
```


```{r}

ggplot(dfDat) + 
  geom_point(aes(x=x,y=y)) + 
  geom_line(aes(x=x,y=yMean)) 

```

#Comare moels and illustrate bias variance tradeoff

## Fit Ridge regression + select lambda with minimum average out-of-fold mse 
```{r}
 
cvFitRidge <- cv.glmnet(x=as.matrix(dfDat[, c("x","const")]),
                      y=as.matrix(dfDat$y),
                      alpha = 0)



dfCoeff <- as.matrix( coef(cvFitRidge,"lambda.min") ) %>% t() %>%  as.data.frame()

dfCoeff

dfPred <- data.frame(x=dfDat$x,
                     predRidge = as.numeric( predict(cvFitRidge, newx= as.matrix(dfDat[, c("x","const")]),s= quantile(cvFitLasso$lambda,0.9) ) )
)
 
dfPred %<>% mutate( predRidgeMin = as.numeric( predict(cvFitRidge, newx= as.matrix(dfDat[, c("x","const")]),s=  cvFitLasso$lambda.min) ) )

dfPred %<>% mutate( predVerif = dfCoeff$`(Intercept)` + dfCoeff$x* x)
dfPred %<>% mutate( diff = predRidge - predVerif)

dfPred %>% head

dfPred %>% filter(diff >0)


dfPred %<>% select(-c(predVerif,diff))

```



## Also lasso for reference
```{r}


cvFitLasso <- cv.glmnet(x=as.matrix(dfDat[, c("x","const")]),
                      y=as.matrix(dfDat$y),
                      alpha = 1)

coef(cvFitLasso, s=median(cvFitLasso$lambda))

#Lambda with sminimal error tends to be small -> not a huge difference with ols
dfPred %<>% mutate(  predLasso = as.numeric( predict(cvFitLasso, newx= as.matrix(dfDat[, c("x","const")]), s=quantile(cvFitLasso$lambda,0.9) ) ) )

dfPred %>% head

```


## OLS
```{r}


lmFit <- glm( y~x, data = dfDat)

dfPred %<>% mutate(predOLS =predict(lmFit))

```

 

```{r}

dfPredMelted <- dfPred %>% melt(id.vars=c("x"))

ggplot(dfDat) + 
  geom_point(aes(x=x,y=y)) + 
  geom_line(aes(x=x,y=yMean)) +
  geom_point(data=dfPredMelted, aes(x=x,y=value,col=variable ))

```

## Bias variance decomposition

```{r}

 
#Fitted ridge coefficient given data X
#(X'X + lambda I)^-1 X'E[y]
lambda <- quantile(cvFitRidge$lambda,0.9)
lambdaMin <- cvFitRidge$lambda.min

X <- dfDat[, c("x","const")] %>% as.matrix()


expRidgeCoeff <- solve ( t(X)%*%X+ lambda * diag(2) ) %*% t(X) %*% dfDat$yMean
expRidgePred <- X %*%expRidgeCoeff

expRidgeMinCoeff <- solve ( t(X)%*%X+ lambdaMin * diag(2) ) %*% t(X) %*% dfDat$yMean
expRidgeMinPred <- X %*% expRidgeMinCoeff

expOLSCoeff <- solve ( t(X)%*%X  ) %*% t(X) %*% dfDat$yMean
expOLSPred <- X %*% expOLSCoeff

varRidge <- sum(( dfPred$predRidge - expRidgePred )**2)  %>% as.numeric()
biasSqRidge <- sum((dfDat$yMean - dfPred$predRidge  )**2)   %>% as.numeric()

varRidgeMin <- sum(( dfPred$predRidgeMin - expRidgeMinPred )**2)  %>% as.numeric()
biasSqRidgeMin <- sum((dfDat$yMean - dfPred$predRidgeMin  )**2)   %>% as.numeric()

varOLS <-  sum(( dfPred$predOLS - expOLSPred)**2)  %>% as.numeric()
biasSqOLS <- sum((dfDat$yMean - dfPred$predOLS )**2)  %>% as.numeric()

mseLessIrreducibleRidge <- varRidge + biasSqRidge 
mseLessIrreducibleRidgeMin <- varRidgeMin + biasSqRidgeMin 
mseLessIrreducibleOLS <- varOLS + biasSqOLS 


mseLessIrreducibleRidge 
mseLessIrreducibleRidgeMin
mseLessIrreducibleOLS
```