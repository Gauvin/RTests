---
title: "multicollinearity"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


#Librairies
```{r}
 
library(magrittr)
library(tidyverse)
library(tidyverse) 
library(broom)
library(reshape2)
library(here)
```


```{r}

#' Create an artifical dataset with 2 continuous and correlated covariates and a continuous response  
#'
#' x2 = alpha x1 + e
#' y = beta1*x1 + beta2*x2 + eps
#'  
#' 
#' @param rho : the desired correlation between the covariates 
#' @param sdX : standard dev of first covariate, (sd of second covar is: alpha sdX)
#' @param sdEps: sd of eps in: y = beta1*x1 + beta2*x2 + eps
#' @param alpha : positive real coeff : x2 = alpha x1 + e
#' @param beta1 : coeff of first covar
#' @param beta2 : coeff of second covar
#' @param numPoints : number of observations
#'
#' @return df data frame with colnames "x1"  "e"   "rho" "x2"  "y"  "eps" 
#' @export
#'
#' @examples
generateReg <- function(rho,     
                        sdX= 3.5,
                        sdEps=0.5,
                        alpha = 5,
                        beta1 = 0.4,
                        beta2 = 1.9,
                        numPoints = 500){
  

  
  varX <- sdX^2
  varE <- (alpha^2*sdX^2*(1-rho^2))/(rho^2)
  sdE <- varE^0.5
  
  df <- data.frame(x1= rnorm(numPoints,sd = sdX),
                   e=rnorm(numPoints,sd=sdE),
                   rho=rep(rho,numPoints),
                   numPoints=rep(numPoints,numPoints),
                   eps = rnorm(numPoints,sd=sdEps))
  
  df %<>% mutate(  x2=alpha*x1 + e,
                   y=beta1*x1+beta2*x2 + eps
                   )

  return(df)
}


```

#Generate 2 perfectly correlated covariates
#The true relationship is y = beta1 x1 + beta2 x2
#since  x2 = alpha x1 + e, 
#y = (beta1 + beta2*alpha) x1 + beta2 e, which is (beta1 + beta2*alpha) x1 for small e 
```{r}

beta1 <- 0.5
beta2 <- 2
alpha <- 5


df <- generateReg(rho = 1,
                  beta1 = beta1,
                  beta2 = beta2,
                  alpha=alpha)

df %>% head


ggplot(df) + 
  geom_point(aes(x=x1, y= x2))
```

#With perfectly correlated covariates, the model is not identifiable
```{r}

lmFit <- lm( y~x1+x2 , data=df)
lmFit %>% summary

lmFitParsimonious <- lm( y~x1 , data=df)
lmFitParsimonious %>% summary

broom::tidy(lmFitParsimonious) %>% filter(term=="x1") %>% pull(estimate) #estimated
beta1 + beta2*alpha #true
```

#What about the prediction now
```{r}

df %<>% mutate( predLM = predict(lmFit) )
df %<>% mutate( predLMParsimonious = predict(lmFitParsimonious) )

df %<>% mutate( residLM = residuals(lmFit) )
df %<>% mutate( residLMParsimonious = residuals(lmFitParsimonious) )

dfMelt <- df %>% select( matches(".*pred.*|y")) %>% melt(id.var="y")


ggplot(dfMelt) +
  geom_point(aes(x=value,y=y,color=variable)) + 
  facet_wrap(~variable)

```

#Check that for Gaussian GLM the raw, working, pearson and deviance residuals all match
```{r}

dfResid <- data.frame(y=df$y)
dfResid %<>% mutate( residManual = y-predict(lmFit) )
listResids <- c("working", "response", "deviance", "pearson")
for( v in listResids){
  dfResid[[v]] <-  residuals(lmFit, type=v) 
}

dfMelted <- dfResid %>% select(-y) %>% melt(id.vars="residManual")
  
ggplot(dfMelted) + 
  geom_point(aes(x=residManual, y = value,col=variable))
  

```