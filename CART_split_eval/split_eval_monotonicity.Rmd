---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

 

# Setup
## Librairies 

```{r}

library(tidyverse)
library(magrittr)
library(here)
library(glue)


```


---
---


# data gen 

```{r}

n <- 10**3
sd <- 3

logit <- function(x) (1+exp(-x))**-1

df <- data.frame( obs=1:n,
                  norm_rv= rnorm(n = n, sd = sd ),
                  poisson_infl_rv = rpois(n, sd**2) )


df %<>% mutate( is_zero = logit(norm_rv/sd) < 0.5)
df %<>% mutate( poisson_infl_rv = ifelse(is_zero, 0,  poisson_infl_rv))



df_long <- df %>% select( -is_zero) %>% pivot_longer(-obs)


ggplot(df_long) + 
  geom_density(aes(x=value)) + facet_wrap(~name)

```



```{r}


compute_node_variance <- function(x, thresh , min_node_size=5 ){
  
  x <- x[!is.na(x)]
  x <- unique(x)
  
  x_left  <- x[ x <thresh]
  x_right <- x[ x >=thresh]
  
  if(length(x_left) < min_node_size ||  length(x_right) < min_node_size){
    return(NA)
  }

  var_total <-  var(x_left) + var(x_right)
  
  return(var_total)
}




df %<>% arrange(norm_rv)

min_nodes <- 5
thresh_indices <- seq(min_nodes+1, n-min_nodes)
thresh_list <- df$norm_rv[ thresh_indices ] #n-2k elements: remove k from each tail

df$var_norm <-  map_dbl( df$norm_rv,  ~ compute_node_variance(df$norm_rv, .x, min_nodes) )
df$var_poisson <-  map_dbl( df$poisson_infl_rv,  ~ compute_node_variance(df$poisson_infl_rv, .x, min_nodes) )

assertthat::assert_that( is.na( compute_node_variance(df$norm_rv, df$norm_rv[[min_nodes]], min_nodes) ))
assertthat::assert_that( is.na( compute_node_variance(df$norm_rv, df$norm_rv[[n-min_nodes+2]], min_nodes) ))




df_poisson <- df %>% select(obs,poisson_infl_rv ,var_poisson) %>% set_names(c("obs","x","var_thresh")) %>% mutate(type="poisson")
df_norm <- df %>% select(obs,norm_rv ,var_norm)%>% set_names(c("obs","x","var_thresh"))%>% mutate(type="norm")

ggplot(rbind(df_poisson,df_norm )) + 
  geom_point(aes(x=x,y=var_thresh)) + 
  facet_wrap(~type )

```


