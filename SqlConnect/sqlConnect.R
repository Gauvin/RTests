library(RODBC)

PSAD <- odbcDriverConnect('driver={SQL Server}; server=NATR08\\DW2K12D; database=CMOSA_Insurance_Property; trusted_connection=true')


query(PSAD, "select a.[sal_ass_prop_id]
/*,a.[rec_create_ts] */
    /*,a.[db_cluster_id]*/
    /*,a.[cmhc_acct_nbr]*/
    /*,a.[slng_prc_src_cde]*/
    /*,a.[pin_supplier_id]*/
    /*,a.[pin_sale_id]*/
    ,a.[sal_prc_eff_dt]
    ,a.[prop_slng_prc_amt]
    /*,a.[sal_clos_dt]*/
    /*,a.[rec_efdt]*/
    /*,a.[rec_lefdt]*/
    /*,a.[rec_delete_ts]*/
    /*,a.[rec_update_uid]*/
    /*,a.[rec_update_ts]*/
    ,b.[geo_latitude_dta]
    ,b.[geo_longitude_dta]
    ,b.lambert_geox
    ,b.lambert_geoy
    ,b.[prop_ad_muni_nm]
    ,b.[met_mjr_cde]
    /*,b.[sc_prov_cde]*/
    from (select * from [CMOSA_Insurance_Property].[dbo].[tbpi_prop_sale] where ([rec_lefdt] = '9999-12-31' and [rec_delete_ts] = '9999-12-31 23:59:59.9970000')) as a
    join
    (select * from [CMOSA_Insurance_Property].[dbo].[tbpi_geo_address]) as b
    on a.sal_ass_prop_id = b.sal_ass_prop_id
    order by a.sal_ass_prop_id")
close(PSAD)