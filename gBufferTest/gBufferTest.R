

## Create a spatial point with a 10km buffer ##
myPoint <- SpatialPoints( as.data.frame( t(c(-77.862755542504,    45.070236947304))) , proj4string =  CRS(proj4string(AD_shp) ) )


myPoint2 <- spTransform(myPoint, CRSobj = CRS("+init=EPSG:3161" ) )
myBuff <- gBuffer(myPoint2,width = 10**4  )

myBuff2 <- spTransform(myBuff, CRSobj =  CRS(proj4string(AD_shp)) )
                       


## Determine which polygons intersect the point ##

matrixResults <- gIntersects(myBuff2, AD_shp   , byid = T)

indicesTouch  <- which( matrixResults == 1 )
polygonsTouch <- row.names(matrixResults)[indicesTouch]
adTouch <- AD_shp[indicesTouch,]





## Determine if the polygons is withint 10 km of the centroid of the polygons ##

distancesFromCentroid <- sapply( 1:length(AD_shp@polygons), function(k) {
        distm(x = as.data.frame( t(c(-77.862755542504, 45.070236947304))), 
              y = as.data.frame( t(AD_shp@polygons[[k]]@labpt)),
              fun = distHaversine
      )
})

distancesFromCentroidLess10km <- distancesFromCentroid[ distancesFromCentroid <= 10**4 ]

indicesCentroidLess10km <- which( distancesFromCentroid <= 10**4  ) 

adCentroidLess10km  <- AD_shp[indicesCentroidLess10km,]


## Get the ads with centroid close OR touching ##


indicesTouchOrCentroidLess10km  <- union_all(indicesTouch, indicesCentroidLess10km)
adTouchOrCentroidLess10km <- AD_shp[indicesTouchOrCentroidLess10km,]



## Leaflet plot ##

myLeaflet <- leaflet( ) %>% addProviderTiles('Esri.WorldGrayCanvas') %>% 

  addPolygons(
    data        = adTouch,
    fillColor   = "green",
    fillOpacity = 0.2,
    color       = "grey",
    weight      = 1.5,
    group = "touches" , 
    highlightOptions = highlightOptions(color = "white", weight=2)
    
  ) %>%
  
  addPolygons(
    data        = adCentroidLess10km,
    fillColor   = "blue",
    fillOpacity = 0.2,
    color       = "grey",
    weight      = 1.5,
    group = "centroid close",
    highlightOptions = highlightOptions(color = "white", weight=2)
    
  ) %>% 
  
  addCircles(data   = adTouchOrCentroidLess10km,
             lng = ~Longitude_AD,
             lat = ~Latitude_AD,
             weight = 10,
             radius = 100, #Radius is in meters
             group  = "both" ,
             color = "orange",
             fillOpacity = 1
  ) %>% 

  
  # Add a small circle for ADs
  addPolygons(data   = myBuff2,
             color = "red",
             group="point",
             fillOpacity = 0.3
  )   %>% 
  
  
  addCircles(data   = myPoint,
              color = "red",
              group="point",
              fillOpacity = 0.5
  )   %>% 
  
  addLayersControl(
    overlayGroups= c("centroid close","touches","both","point") ,
    options= layersControlOptions(collapsed=F)
  )

(myLeaflet)
