
set.seed(100)

library(rlang)
library(miceadds)
library(rlist)

library(Hmisc) #for the weighted wtd.v  

library(igraph)

library(magrittr)
library(dplyr)
library(tidyr)

library(R6)

library(rgeos)
library(sp)
library(geosphere)

library(leaflet)
library(RColorBrewer)
library(htmlwidgets)



## Get the DAs ##
load.Rdata( filename=  "I:/Recherche actuarielle/Projets/Projets/T34748_AgencyFootprint_Clustering/Data/RDATA/AD_shp.RDATA" , 
            objname = "AD_shp" 
)

## Get the agents with valid longitude/latitude ##

dfAgentsHistorical <- read.csv(file="I:/Recherche actuarielle/Projets/Projets/T34748_AgencyFootprint_Clustering/Data/Csv/agent_location_201706_ON_FINAL.csv", sep=";")

#Filter out NB and AB

dfGroupedByAgent <-  dfAgentsHistorical %>% 
  filter(PROVINCEAGENT == "ON") %>% 
  group_by( AWARDSID ) %>% 
  summarise( Longitude_agent=max(Longitude_agent), Latitude_agent=max(Latitude_agent) )

dfGroupedByAgent %<>% filter( Longitude_agent != 0)

(dfGroupedByAgent %>% nrow)


## Create a spatial point with a 10km buffer ##
myPoint <- SpatialPoints( dfGroupedByAgent[, c("Longitude_agent","Latitude_agent")], proj4string =  CRS(proj4string(AD_shp) ) )


myPoint2 <- spTransform(myPoint, CRSobj = CRS("+init=EPSG:3161" ) )
myBuff <- gBuffer(myPoint2,width = 10**4 ,byid = T )

myBuff2 <- spTransform(myBuff, CRSobj =  CRS(proj4string(AD_shp)) )
                       
#plot(myBuff2)

## Determine which polygons intersect the point ##

matrixResults <- gIntersects(myBuff2, AD_shp   , byid = T)

indicesTouch  <- which( matrixResults == 1 )
polygonsTouch <- row.names(matrixResults)[indicesTouch]





indicesTouch <- sapply( 1:length(myPoint), function(l) which( matrixResults[,l] == 1 ) )
  
adTouch <- AD_shp[unique( unlist(indicesTouch) ) ,]



## Determine if the polygons is withint 10 km of the centroid of the polygons ##
#distancesFromCentroid <- vector("list",length(myPoint))
# for(l in 1:length(myPoint)){
#   distancesFromCentroid[[l]] <- sapply( 1:length(AD_shp@polygons), function(k) {
#           distm(x = dfGroupedByAgent[l, c("Longitude_agent","Latitude_agent")], 
#                 y = as.data.frame( t(AD_shp@polygons[[k]]@labpt)),
#                 fun = distHaversine
#         )
#   })
#   
#   indicesCentroidLess10km[[l]] <- which( distancesFromCentroid[[l]] <= 10**4  ) 
# }


dfCentroids <- sapply(1:length(AD_shp@polygons), function(k) AD_shp@polygons[[k]]@labpt  )
dfCentroids <- as.data.frame( t(as.matrix(dfCentroids)) )
colnames(dfCentroids) <- c("Longitude_AD","Latitude_AD")
   
distancesFromCentroid <- distm(dfCentroids, dfGroupedByAgent[, c("Longitude_agent","Latitude_agent")] , fun = distCosine)

indicesCentroidLess10km <- lapply( 1:ncol(distancesFromCentroid), function(l) which( distancesFromCentroid[ , l] <= 10**4  ) )

adCentroidLess10km  <- AD_shp[unique(unlist(indicesCentroidLess10km)),]


## Get the ads with centroid close OR touching ##


indicesTouchOrCentroidLess10km  <- union_all(unique( unlist(indicesTouch) ) , unique(unlist(indicesCentroidLess10km)))
adTouchOrCentroidLess10km <- AD_shp[indicesTouchOrCentroidLess10km,]



## Determine which DAs are never touched nor close to agency##
indicesNotTouchedNorClose <- setdiff( 1:nrow(AD_shp), indicesTouchOrCentroidLess10km )
adNotTouchedNorClose <- AD_shp[indicesNotTouchedNorClose,]

## Leaflet plot ##

myLeaflet <- leaflet( ) %>% addProviderTiles('Esri.WorldGrayCanvas') %>% 
# 
#   addPolygons(
#     data        = adTouch,
#     fillColor   = "green",
#     fillOpacity = 0.2,
#     color       = "grey",
#     weight      = 1.5,
#     group = "touches" , 
#     highlightOptions = highlightOptions(color = "white", weight=2)
#     
#   ) %>%
  
  addPolygons(
    data        = adCentroidLess10km,
    fillColor   = "blue",
    fillOpacity = 0.2,
    color       = "grey",
    weight      = 1.5,
    group = "centroid close",
    highlightOptions = highlightOptions(color = "white", weight=2)
    
  ) %>% 
  
  addPolygons(
    data        = adNotTouchedNorClose,
    fillColor   = "purple",
    fillOpacity = 0.2,
    color       = "grey",
    weight      = 1.5,
    group = "not touching nor close",
    highlightOptions = highlightOptions(color = "white", weight=2)
    
  ) %>% 
  
  
  # addCircles(data   = adTouchOrCentroidLess10km,
  #            lng = ~Longitude_AD,
  #            lat = ~Latitude_AD,
  #            weight = 10,
  #            radius = 100, #Radius is in meters
  #            group  = "both" ,
  #            color = "orange",
  #            fillOpacity = 1
  # ) %>% 

  
  # Add a small circle for ADs
  addPolygons(data   = myBuff2,
             color = "red",
             group="point",
             fillOpacity = 0.3
  )   %>% 
  
  
  # addCircles(data   = myPoint,
  #             color = "red",
  #             group="point",
  #             fillOpacity = 0.5
  # )   %>% 
  
  addLayersControl(
    overlayGroups= c("centroid close","touches","both","point","not touching nor close") ,
    options= layersControlOptions(collapsed=F)
  )

(myLeaflet)
