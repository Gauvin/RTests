
###1D ####

indThreshold <- function(x1,b2=8, a=6){
  y <- 0
  if(x1<a) y <- y + b2
  return(y)
}

indInterval <- function(x1,b2=8, a1=6 ,a2=8 ){
  stopifnot(a1<=a2)
  
  y <- 0
  if(x1>=a1 & x1 <a2) y <- y + b2
  return(y)
}

## linear fct


linearFct <- function(x1,b1=10){
  return(b1*x1)
}

discontFct <- function(x1,baseLinear=linearFct,baseInd=indThreshold){
  y <- baseLinear(x1)  + baseInd(x1)
  return(y)
}



###2D ####

disc2DFct <- function(x1,x2){
  
  #This partitions the x1,x2 space into 2^2 disjoint polyhedrons 
  # x1 < 10 x1 >= 10             x2 < 5 x2 >=5
  y <-  indThreshold(x1, a=10,b=3 ) + indThreshold(x2, a=5,b=5 ) 
}


disc2DFctBumpier <- function(x1,x2){
  
  y <- disc2DFct(x1,x2)
  
  #This partitions the x1,x2 space into 2^2 disjoint polyhedrons 
  # x1 < 10 x1 >= 10             x2 < 5 x2 >=5
  y <- y + indInterval(x1, b2=13, a1=6 ,a2=8 ) + 
    indInterval(x1, b2=7, a1=1 ,a2=4 ) + 
    indInterval(x2, b2=17, a1=2 ,a2=5 ) +
    indInterval(x2, b2=11, a1=10 ,a2=14 ) +
    indInterval(x1, b2=11, a1=10 ,a2=14 )
}


### Utils ###


buildSurface <- function(surfaceFun, domain=seq(1,15,by=1)){
  
  dfDomainx1 <- data.frame(x1=domain,
                           dummy=rep(1,length(domain)))
  
  dfDomainx2 <- data.frame(x2=domain,
                           dummy=rep(1,length(domain)))                      
  
  dfLongSurf <- left_join(dfDomainx1, dfDomainx2, by="dummy" )
  
  dfLongSurf$y <- map_dbl( 1:nrow(dfLongSurf) , ~surfaceFun(dfLongSurf$x1[[.x]], dfLongSurf$x2[[.x]] ) )
  
  
  matSurf <- matrix(data = dfLongSurf$y, nrow=nrow(dfDomainx1), ncol=nrow(dfDomainx2), byrow = T)  
  rownames(matSurf) <- dfDomainx1$x1
  colnames(matSurf) <- dfDomainx2$x2
  
  return(list(matSurf=matSurf,
              dfLongSurf=dfLongSurf))
}




#### Variance/density in region ####



getVarDummyFun <- function(dfLongSurf){
  
  #Smooth the x1>8 part more 
  #Take a variance 10X smaller for sharper edges in the half space: x1>8
  dfLongSurf %<>% mutate( var = ifelse( x1 > 8 , 0.1 , 1 ) )
  
  return(dfLongSurf)
}
