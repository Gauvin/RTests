 

### Nd ###
localRegND <- function(x0, kernelFct,dfobsx ,obsy){
  
  #Get the weights (we could use the exact same weights as the nadaraya-watson, since these are
  #exactly equal up to a constant multiple and lm will adjust coefficients accordingly
  weights <- map_dbl(1:nrow(dfobsx), ~  kernelFct(x0, dfobsx[.x, ] ))
  
  #Create the df for linear pred 
  #use all the columns in dfobsx, so we should probably make sure we have no garbage in there
  print(paste0("Using ", ncol(dfobsx), " predictors for local reg in localRegND"))
  colnames(dfobsx) <- paste0("x",1:ncol(dfobsx))
  dfReg <- data.frame( y=obsy) %>% bind_cols(dfobsx)
  linRegResults <- lm(y ~ . , weights= weights, data=dfReg)
  
  #Get the linear coeff functions of x0
  #Then take inner product with intercept + x0
  predX0 <- c(1, as.matrix(x0)) %*% linRegResults$coefficients
  
  return( c(predX0) ) #force to numeric 
  
  
}


smoothLocalRegNDFct <- function(dfLongSurf, ndKernel, strKernelSmooth){
  
  dfobsx <- dfLongSurf %>% dplyr::select(x1,x2)
  
  
  kernelSmoothed2D <- map_dbl(1:nrow(dfobsx) , ~ localRegND( dfobsx[.x, ], ndKernel, dfobsx , dfLongSurf$y ) )   
  
  dfLongSurf[[strKernelSmooth]] <- kernelSmoothed2D
  
  dfLongSurf[[paste0("diff",strKernelSmooth)]] <-  abs(dfLongSurf[[strKernelSmooth]]-dfLongSurf$y)
  
  
  matSmoothed <- matrix(data = kernelSmoothed2D, 
                        nrow=sqrt(length(kernelSmoothed2D)), 
                        ncol=sqrt(length(kernelSmoothed2D)), byrow = T)  
  rownames(matSmoothed) <- unique(dfLongSurf$x1)
  colnames(matSmoothed) <- unique(dfLongSurf$x2)
  
  return(list(dfLongSurf=dfLongSurf,
              matSmoothed=matSmoothed))
}


#### Spatially varying variance 


nadarayaWatsonNDSpatialVar <- function(x0,  dfobsx ,obsy, obsVars){
  
  weights <- map_dbl(1:nrow(dfobsx), ~    gaussianDensity(x0-dfobsx[.x, ], obsVars[[.x]])  )
  weightsNormalized <- weights/sum(weights)
  
  return( c(weightsNormalized %*% obsy) ) #use c to coerce to numeric
  
}

 



smoothNadWatsonNDFctSpatialVar <- function(dfLongSurf,  strKernelSmooth){
  
  dfobsx <- dfLongSurf %>% dplyr::select(x1,x2)
  
  kernelSmoothed2D <- map_dbl(1:nrow(dfobsx) , ~ nadarayaWatsonNDSpatialVar( dfobsx[.x, ], dfobsx , dfLongSurf$y, dfLongSurf$var ) )   
  
  dfLongSurf[[strKernelSmooth]] <- kernelSmoothed2D
  
  dfLongSurf[[paste0("diff",strKernelSmooth)]] <-  abs(dfLongSurf[[strKernelSmooth]]-dfLongSurf$y)
  
  
  matSmoothed <- matrix(data = kernelSmoothed2D, 
                        nrow=sqrt(length(kernelSmoothed2D)), 
                        ncol=sqrt(length(kernelSmoothed2D)), byrow = T)  
  rownames(matSmoothed) <- unique(dfLongSurf$x1)
  colnames(matSmoothed) <- unique(dfLongSurf$x2)
  
  return(list(dfLongSurf=dfLongSurf,
              matSmoothed=matSmoothed))
}
