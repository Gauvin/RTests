---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


#Librairies
```{r}
 
library(magrittr)
library(tidyverse) 
library(stopwords)
library(reshape2)
library(ranger)
library(glue)

```
# Generate dataset

```{r}

numObs <- 100
df <- data.frame(x=runif(numObs),
                 y=rbinom(n = numObs,size=10,p=0.1))

df %>% head

df$idx <- 1:nrow(df)

```


#Create the test folds
```{r}

numFolds <- 7
sizeFold <- floor(numObs/numFolds)


listTestFolds <- list()
listTrainFolds <- list()
for(k in 1:numFolds){
  
  start <- (k-1)*sizeFold+1
  end <- ifelse(k == numFolds,  nrow(df), k*sizeFold)
  rangeTest <- seq(from=start,end )
  rangeTrain <- 1:nrow(df) 
  rangeTrain <- rangeTrain[ !(rangeTrain %in% rangeTest)]
  
  listTestFolds <- rlist::list.append(listTestFolds, rangeTest)
  listTrainFolds <- rlist::list.append(listTrainFolds, rangeTrain)
}


```


#Compute lin reg
```{r}

#Permute rows
rowsPermuted <- sample(df$idx ,size = nrow(df),replace = F)
dfPerm <- df[rowsPermuted, ]

mse <- 0
for (k in 1:length(listTrainFolds)){
  
  trainSet <- listTrainFolds[[k]]
  testSet <- listTestFolds[[k]]
  
  datTrain= dfPerm[trainSet, ]
  datTest= dfPerm[testSet, ]
  
  lmFit <- lm( y~x, data=datTrain)
  
  mseFold <- sum((datTest$y - predict(lmFit, newdata= datTest))**2)**0.5
  mse <- mse + mseFold
}


mse <- mse/length(listTrainFolds)

mse
```

```{r}

sum( (df$y - predict(lm( y~x, data=df)))**2 ) **0.5


```