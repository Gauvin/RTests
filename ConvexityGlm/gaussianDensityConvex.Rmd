---
title: "Gaussian distribution and convexity"
author: "Charles"
date: "March 31, 2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


#Maximum likelihood

We collect $i$ independent observations that follow a Gaussian distribution with unknow parameters:

$x_i \sim \mathcal{N}(\mu,\sigma^2), i=1,\cdots,n$

We seek to infer $\mu$ and $\sigma^2$ from the data by maximizing the log likelihood

$$
\begin{align}
\mu^* &= \arg\max_{\mu} \sum_i \log( (2\pi\sigma^2)^{-1/2} e^{-\frac{(x_i-\mu)^2}{2\sigma^2}}) \\
  & = \arg\max_{\mu} -\sum_i \frac{1}{2} \log( (2\pi\sigma^2)-\sum_i \frac{(x_i-\mu)^2}{2\sigma^2} \\ 
& = \frac{\sum_i x_i}{n}
\end{align}
$$
since $-\sum_i \frac{1}{2} \log( (2\pi\sigma^2)-\sum_i \frac{(x_i-\mu)^2}{2\sigma^2}$ is concave in $\mu$ (concave quadratic). To infer the variance $\sigma^2$, we first consider

$$
\begin{align}
\sigma^{2*} &= \arg\max_{\sigma^{2}} \sum_i \log( (2\pi \sigma^{2})^{-1/2}  e^{-\frac{(x_i-\mu)^2}{2\sigma^2}}) 
\end{align}
$$

The log-likelhihood is *not* concave in $\sigma^{-2}$, but it *is* concave in $\sigma$ and $\theta=\sigma^{-2}$ (these mappings are a bijections since $\sigma>0$). Using $\theta=\sigma^{-2}$ (the inverse of the covariance matrix is called the precision matrix), we get:

$$
\begin{align}
 \theta ^* & = \arg\max_{\theta = \sigma^{-2}} -\sum_i \frac{1}{2} \log(2\pi) + \frac{n}{2} \log(\theta) -\sum_i \frac{(x_i-\mu)^2}{2} \theta\\ 
  &=  \sum_i \frac{n }{(x_i-\mu)^2} 
\end{align}
$$

which we soved by setting the derivative wrt $\theta$ equal to zero. We then get $\sigma^{2*}= \theta^{*-1} =  \sum_i \frac{(x_i-\mu)^2 }{n}$

  