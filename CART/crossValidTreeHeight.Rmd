---
title: "Untitled"
output: html_document
---

#Librairies

```{r, message=F, echo=F, include=F,error=FALSE}

library(magrittr)
library(tidyverse)
library(ggplot2)
 
library(glue)
 
library(caret)

library(rpart)
 
set.seed(1)

```


# Evaluate best tree height with 10 fold cross validation

```{r}

train.control <- trainControl(method = "cv", number = 10)

# Train the model
model <- train(Species ~., data = iris, 
               method = "rpart2",
               tuneGrid = data.frame(maxdepth = 1:5),
               trControl = train.control)

```

# Inspect results
```{r}

dfCv <- model$results %>% as.data.frame()

ggplot(dfCv) + 
  geom_errorbar(aes(x=maxdepth, ymin = Accuracy-AccuracySD  , ymax=Accuracy+AccuracySD))


```