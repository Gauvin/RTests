DATA<- read.table("C:\\Users\\chaimae\\Desktop\\Essai\\DATA.csv",sep=",",header=T)
rta_shp_qc <- readRDS("C:\\Users\\chaimae\\Desktop\\Essai\\rta_shp_qc.rds")

library(dplyr)
library(tidyr)
library(leaflet)
library(factoextra)
library(FactoMineR)
library(plotly)
library(sf)

PHONE_COST_PER_CAPITA <- DATA %>% 
  # on filtre sur l'annee 2017, les semaines de 8 a 16
  # on retire les RTA sans population
  filter(GEO_POP> 0,YEAR == 2017, WEEKNUM %in% 8:16) %>% 
  # on cree le nombre de telemarketing par population
  mutate(
    PHONE__COST_POP = PHONE__COST / GEO_POP
  ) %>% 
  # avec la fonction spread, on obtient une ligne par RTA, et autant de colonnes que de semaines
  select(WEEKNUM, GEO_RTA, GEO_NBF_2016, GEO_POP, PHONE__COST_POP) %>% 
  group_by(GEO_RTA, GEO_POP) %>% 
  spread(WEEKNUM, PHONE__COST_POP) %>% 
  ungroup()

PHONE_COST_PER_CAPITA$MEAN <- rowMeans(PHONE_COST_PER_CAPITA[,4:12])
PHONE_COST_PER_CAPITA$SUMS <- rowSums(PHONE_COST_PER_CAPITA[,4:12])
PHONE_COST_PER_CAPITA$REGION <- ifelse(grepl(0, PHONE_COST_PER_CAPITA$GEO_RTA) == T, 'RURAL', 'URBAIN')

SOUM_PER_CAPITA <- DATA %>% 
  # on filtre sur l'annee 2017, les semaines de 8 a 16
  # on retire les RTA sans population
  filter(GEO_POP> 0,YEAR == 2017, WEEKNUM %in% 8:16) %>% 
  # on cree le nombre de telemarketing par population
  mutate(
    SOUM_POP = SOUM / GEO_POP
  ) %>% 
  # avec la fonction spread, on obtient une ligne par RTA, et autant de colonnes que de semaines
  select(WEEKNUM, GEO_RTA, GEO_NBF_2016, GEO_POP, SOUM_POP) %>% 
  group_by(GEO_RTA, GEO_POP) %>% 
  spread(WEEKNUM, SOUM_POP) %>% 
  ungroup()

SOUM_PER_CAPITA$MEAN <- rowMeans(SOUM_PER_CAPITA[,4:12])
SOUM_PER_CAPITA$SUMS <- rowSums(SOUM_PER_CAPITA[,4:12])
SOUM_PER_CAPITA$REGION <- ifelse(grepl(0, SOUM_PER_CAPITA$GEO_RTA) == T, 'RURAL', 'URBAIN')


# URBAIN VS RURAL
sum(PHONE_COST_PER_CAPITA$REGION=="URBAIN")  # 379
sum(PHONE_COST_PER_CAPITA$REGION=="RURAL")   # 40

RURAL_COST_PHONE <- PHONE_COST_PER_CAPITA  %>% 
  filter(REGION=="RURAL") %>% 
  ungroup()

URBAIN_COST_PHONE <- PHONE_COST_PER_CAPITA  %>% 
  filter(REGION=="URBAIN") %>% 
  ungroup()

# Donn�es Stat Can - Kmeans 
StatCan_Data$REGION <- ifelse(grepl(0, StatCan_Data$GEO_RTA) == T, 'RURAL', 'URBAIN')
SCDCluster <- kmeans(StatCan_Data[, 3:44], 3, nstart = 20)
StatCan_Data$Cluster <- SCDCluster$cluster

RTA_Cluster_1 <- StatCan_Data$GEO_RTA[StatCan_Data$Cluster==1]

index_RTA_Cluster_1 <- which(PHONE_COST_PER_CAPITA$GEO_RTA %in% RTA_Cluster_1)
Phone_Cluster_1 <- PHONE_COST_PER_CAPITA[index_RTA_Cluster_1 ,]

## ACP
data <- Phone_Cluster_1 %>% 
  filter(`8`  < min(boxplot.stats(Phone_Cluster_1$`8`)$out) ) %>% 
  filter(`9`  < min(boxplot.stats(Phone_Cluster_1$`9`)$out) ) %>% 
  filter(`10` < min(boxplot.stats(Phone_Cluster_1$`10`)$out) ) %>% 
  filter(`11` < min(boxplot.stats(Phone_Cluster_1$`11`)$out) ) %>% 
  filter(`12` < min(boxplot.stats(Phone_Cluster_1$`12`)$out) ) %>% 
  filter(`13` < min(boxplot.stats(Phone_Cluster_1$`13`)$out) ) %>% 
  filter(`14` < min(boxplot.stats(Phone_Cluster_1$`14`)$out) ) %>% 
  filter(`15` < min(boxplot.stats(Phone_Cluster_1$`15`)$out) ) %>% 
  filter(`16` < min(boxplot.stats(Phone_Cluster_1$`16`)$out) )

sum(data$REGION=="URBAIN")
sum(data$REGION=="RURAL")

pca_PHONE <- PCA(
  X=data[,4:12] ,
  scale.unit = T, 
  graph      = F
)

pca_eigval <- get_eigenvalue(pca_PHONE)
pca_vars <- get_pca_var(pca_PHONE)
pca_ind <- get_pca_ind(pca_PHONE)

fviz_eig(pca_PHONE, addlabels = TRUE)
fviz_pca_var(pca_PHONE, col.var = "cos2",
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),labelsize=4)
fviz_pca_ind (pca_PHONE,  col.ind = "cos2",
              gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
              labelsize = 2)

pca_PHONE_coord <- get_pca_ind(pca_PHONE)$coord %>% 
  as.data.frame() %>% 
  mutate(
    GEO_RTA = data$GEO_RTA[as.numeric(rownames(get_pca_ind(pca_PHONE)$coord))],
    URBAIN  = ifelse(grepl(0, GEO_RTA) == T, 'RURAL', 'URBAIN')
  )


pca_sort_dim1 <- arrange(pca_PHONE_coord,by=Dim.1)

p_RTA_Dim1 <- pca_sort_dim1$GEO_RTA[2:21]
g_RTA_Dim1 <- pca_sort_dim1$GEO_RTA[208:227]
x1 <- colMeans(data[data$GEO_RTA %in% p_RTA_Dim1,4:12])
x2 <- colMeans(data[data$GEO_RTA %in% g_RTA_Dim1,4:12])

library(plotly)
plot_ly() %>% 
  add_trace(y=as.numeric(x1),x=c(8:16),type="scatter",mode="lines",line = list(width = 2),name="D�penses minimales")%>%
  add_trace(y=as.numeric(x2),x=c(8:16),type="scatter",mode="lines",line = list(width = 2),name="D�penses maximales")%>%
  layout(title = "Moyenne des d�penses dans les diff�rentes RTAs") 


plot_ly() %>% 
  add_trace(y=as.numeric(colMeans(SOUM_PER_CAPITA[SOUM_PER_CAPITA$GEO_RTA %in% p_RTA_Dim1,4:12])),x=c(8:16),type="scatter",mode="lines",line = list(width = 2),name="Soumissions minimales")%>%
  add_trace(y=as.numeric(colMeans(SOUM_PER_CAPITA[SOUM_PER_CAPITA$GEO_RTA %in% g_RTA_Dim1,4:12])),x=c(8:16),type="scatter",mode="lines",line = list(width = 2),name="Soumissions maximales")%>%
  layout(title = "Moyenne des soumissions dans les diff�rentes RTAs") 






## Statistiques descriptives
install.packages('resample')
library(resample)
pMeans <- colMeans(StatCan_Data[StatCan_Data$GEO_RTA %in% p_RTA_Dim1,3:44])
gMeans <- colMeans(StatCan_Data[StatCan_Data$GEO_RTA %in% g_RTA_Dim1,3:44])
pStdev <- colStdevs(StatCan_Data[StatCan_Data$GEO_RTA %in% p_RTA_Dim1,3:44])
gStdev <- colStdevs(StatCan_Data[StatCan_Data$GEO_RTA %in% g_RTA_Dim1,3:44])

t.test(StatCan_Data[StatCan_Data$GEO_RTA %in% p_RTA_Dim1,11],StatCan_Data[StatCan_Data$GEO_RTA %in% g_RTA_Dim1,11])

table <- data.frame(pMeans,gMeans,pVars,gVars)

MROI <- read.table("C:\\Users\\chaimae\\Desktop\\Essai\\mroi_data_quebec_CCC.csv",sep=';',header=T)


cor(MROI$GEO_NBF_2016,MROI$PHONE__COST)
cor(MROI$GEO_POP,MROI$PHONE__COST)
cor(MROI$GEO_NBF_2016,MROI$SOUM)
cor(MROI$GEO_POP,MROI$SOUM)
cor(MROI$PHONE__COST,MROI$PHONE__VOLUME)
cor(MROI$GEO_NBF_2016,MROI$PHONE__COST)
cor(MROI$TELE__GRP_REGION,MROI$SOUM)
cor(MROI$RADIO__GRP_REGION,MROI$SOUM)

SALES_PER_CAPITA <- DATA %>% 
  # on filtre sur l'annee 2017, les semaines de 8 a 16
  # on retire les RTA sans population
  filter(YEAR == 2017, WEEKNUM %in% 8:16) %>% 
  # on cree le nombre de telemarketing par population
  mutate(
    SALES_POP = SALES / GEO_POP
  ) %>% 
  # avec la fonction spread, on obtient une ligne par RTA, et autant de colonnes que de semaines
  select(WEEKNUM, GEO_RTA, GEO_NBF_2016, GEO_POP, SALES_POP) %>% 
  group_by(GEO_RTA, GEO_POP) %>% 
  spread(WEEKNUM, SALES_POP) %>% 
  ungroup()


plot_ly() %>% 
  add_trace(y=as.numeric(colMeans(SALES_PER_CAPITA[SALES_PER_CAPITA$GEO_RTA %in% p_RTA_Dim1,4:12])),x=c(8:16),type="scatter",mode="lines",line = list(width = 2),name="SALES minimales")%>%
  add_trace(y=as.numeric(colMeans(SALES_PER_CAPITA[SALES_PER_CAPITA$GEO_RTA %in% g_RTA_Dim1,4:12])),x=c(8:16),type="scatter",mode="lines",line = list(width = 2),name="SALES maximales")%>%
  layout(title = "Moyenne des SALES dans les diff�rentes RTAs") 

EMAIL_PER_CAPITA <- DATA %>% 
  # on filtre sur l'annee 2017, les semaines de 8 a 16
  # on retire les RTA sans population
  filter(YEAR == 2017, WEEKNUM %in% 8:16) %>% 
  # on cree le nombre de RADIOmarketing par population
  mutate(
    EMAIL_PER_CAPITA = EMAIL__COST / GEO_POP
  ) %>% 
  # avec la fonction spread, on obtient une ligne par RTA, et autant de colonnes que de semaines
  select(WEEKNUM, GEO_RTA, GEO_NBF_2016, GEO_POP,EMAIL_PER_CAPITA) %>% 
  group_by(GEO_RTA, GEO_POP) %>% 
  spread(WEEKNUM, EMAIL_PER_CAPITA) %>% 
  ungroup()


plot_ly() %>% 
  add_trace(y=as.numeric(colMeans(EMAIL_PER_CAPITA[EMAIL_PER_CAPITA$GEO_RTA %in% p_RTA_Dim1,4:12])),x=c(8:16),type="scatter",mode="lines",line = list(width = 2),name="Phone minimales")%>%
  add_trace(y=as.numeric(colMeans(EMAIL_PER_CAPITA[EMAIL_PER_CAPITA$GEO_RTA %in% g_RTA_Dim1,4:12])),x=c(8:16),type="scatter",mode="lines",line = list(width = 2),name="Phone maximales")%>%
  layout(title = "Moyenne des d�penses (EMAIL) dans les diff�rentes RTAs") 

