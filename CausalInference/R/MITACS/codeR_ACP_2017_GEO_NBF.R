## Lecture des donn�es  -  Analyse pour l'ann�e 2017 (per capita signifie ici par GEO_NBF)

PHONE_COST_PER_CAPITA <- read.csv("C:\\Users\\Chaimae\\Desktop\\DGAG Work 30.11\\PHONE_COST_PER_CAPITA.txt",header=T,sep=",")
SOUM_PER_CAPITA <- read.csv("C:\\Users\\Chaimae\\Desktop\\DGAG Work 30.11\\SOUM_PER_CAPITA.txt",header=T,sep=",")

PHONE_COST_2015_8 <- PHONE_COST_PER_CAPITA[8:16,]
PHONE_COST_2016_8 <- PHONE_COST_PER_CAPITA[61:62,]
PHONE_COST_2017_8 <- PHONE_COST_PER_CAPITA[113:121,]

SOUM_2015_8 <- SOUM_PER_CAPITA[8:16,]
SOUM_2016_8 <- SOUM_PER_CAPITA[61:62,]
SOUM_2017_8 <- SOUM_PER_CAPITA[113:121,]

#Enlever les points (RTAs) ab�rrants 

View(PHONE_COST_2017_8)
w1 <- PHONE_COST_2017_8[1,3:421]
w2 <- PHONE_COST_2017_8[2,3:421]
w3 <- PHONE_COST_2017_8[3,3:421]
w4 <- PHONE_COST_2017_8[4,3:421]
w5 <- PHONE_COST_2017_8[5,3:421]
w6 <- PHONE_COST_2017_8[6,3:421]
w7 <- PHONE_COST_2017_8[7,3:421]
w8 <- PHONE_COST_2017_8[8,3:421]
w9 <- PHONE_COST_2017_8[2,3:421]

library(plotly)
library(psych)

w9[which(w9 %in%  boxplot.stats(w9)$out[order(boxplot.stats(w9)$out,decreasing=FALSE)] )]

outliers <- c(131,324,307,308,133,137,117,119,113,416,82,120,108,110) #22, 221
colnames(PHONE_COST_2017_8[,outliers+2])
PHONE_NO_Outliers_2017_8 <- PHONE_COST_2017_8[,-c(1,2,22,221,outliers+2)]

## ACP 

library("FactoMineR")
library("factoextra")

PHONE_NO_Outliers_2017_8_t <- t(PHONE_NO_Outliers_2017_8)  
PHONE_NO_Outliers_2017_8_t_PCA_scaled <- PCA(PHONE_NO_Outliers_2017_8_t,scale.unit = TRUE, graph = FALSE)
PHONE_NO_Outliers_2017_8_t_PCA_scaled <- PCA(PHONE_NO_Outliers_2017_8_t,scale.unit = F, graph = FALSE)

eig.val <- get_eigenvalue(PHONE_NO_Outliers_2017_8_t_PCA_scaled)
fviz_eig(PHONE_NO_Outliers_2017_8_t_PCA_scaled, addlabels = TRUE)

var <- get_pca_var(PHONE_NO_Outliers_2017_8_t_PCA_scaled)

coord_var <- as.data.frame(var$coord)

#Graphique des variables

fviz_pca_var(PHONE_NO_Outliers_2017_8_t_PCA_scaled, col.var = "cos2",
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),labelsize=2
             # �vite le chevauchement de texte
)


ind_2017 <- get_pca_ind(PHONE_NO_Outliers_2017_8_t_PCA_scaled)

# Graphique des individus

fviz_pca_ind(PHONE_NO_Outliers_2017_8_t_PCA_scaled,  col.ind = "cos2",
              gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
              labelsize = 2)


# Individus par r�gion

plot_ly(data = Indices_pca_phone_2017, x = ~Dim.1, y = ~Dim.2,
 color = ~REGION, text=~RTA)

# Dim 1 et Dim 2 par r�gion et par RTA

Indices_pca_phone_2017 <- as.data.frame(ind_2017$coord)
Indices_pca_phone_2017$REGION <- ifelse(grepl(0,rownames(Indices_pca_phone_2017))==TRUE,"RURAL","URBAIN")
Indices_pca_phone_2017$RTA <- rownames(Indices_pca_phone_2017)
a <- arrange(Indices_pca_phone_2017,Dim.1)
b <- arrange(Indices_pca_phone_2017,Dim.2)

# Coordonn�es sur les dimensions 1 et 2, plus grandes ,et plus petites, ici, les 20 plus grandes
# et les 20 plus petites

petits_indices_1<-c(a$RTA[1:20])
grands_indices_1 <- c(a$RTA[384:403])
petits_indices_2 <-c(b$RTA[1:20])
grands_indices_2 <- c(b$RTA[384:403])
table <- as.data.frame(petits_indices_1,grands_indices_1)



## Plot des d�penses par RTA pour les 20 plus grandes et plus petites dim1 et dim2
p_dim1_Phone_2017 <- plot_ly()
for(i in which(colnames(PHONE_COST_2017_8)%in%petits_indices_1)){
  p_dim1_Phone_2017  <- add_trace(p_dim1_Phone_2017, x=c(8:16),
                                  y = PHONE_COST_2017_8[,i],
                                  type = 'scatter', 
                                  mode = 'lines+markers', 
                                  line = list(width = 1),
                                  name = colnames(PHONE_COST_2017_8)[i])
}
p_dim1_Phone_2017 %>% add_trace(y=rowMeans(PHONE_COST_2017_8[,petits_indices_1]),x=c(8:16),type="scatter",mode="lines+markers",line = list(width = 4),name="Moyenne des soumissions",color=I("#FC4E07"))%>%
  layout(title = "Petites coordonn�es sur Dim 1: Phone 2017")

g_dim1_Phone_2017 <- plot_ly()
for(i in which(colnames(PHONE_COST_2017_8)%in% grands_indices_1)){
  g_dim1_Phone_2017  <- add_trace(g_dim1_Phone_2017, x=c(8:16),
                                  y = PHONE_COST_2017_8[,i],
                                  type = 'scatter', 
                                  mode = 'lines+markers', 
                                  line = list(width = 1),
                                  name = colnames(PHONE_COST_2017_8)[i])
}
g_dim1_Phone_2017 %>% add_trace(y=rowMeans(PHONE_COST_2017_8[,grands_indices_1]),x=c(8:16),type="scatter",mode="lines+markers",line = list(width = 4),name="Moyenne des soumissions",color=I("#FC4E07"))%>%
  layout(title = "Grandes coordonn�es sur Dim 1: Phone 2017")


g_dim2_Phone_2017 <- plot_ly()
for(i in which(colnames(PHONE_COST_2017_8)%in% grands_indices_2)){
  g_dim2_Phone_2017  <- add_trace(g_dim2_Phone_2017, x=c(8:16),
                                  y = PHONE_COST_2017_8[,i],
                                  type = 'scatter', 
                                  mode = 'lines+markers', 
                                  line = list(width = 1),
                                  name = colnames(PHONE_COST_2017_8)[i])
}
g_dim2_Phone_2017   %>% add_trace(y=rowMeans(PHONE_COST_2017_8[,grands_indices_2]),x=c(8:16),type="scatter",mode="lines+markers",line = list(width = 4),name="Moyenne des soumissions",color=I("#FC4E07"))%>%
  layout(title = "Grandes coordonn�es sur Dim 2: Phone 2017") 

p_dim2_Phone_2017 <- plot_ly()
for(i in which(colnames(PHONE_COST_2017_8)%in% petits_indices_2)){
  p_dim2_Phone_2017  <- add_trace(p_dim2_Phone_2017, x=c(8:16),
                                  y = PHONE_COST_2017_8[,i],
                                  type = 'scatter', 
                                  mode = 'lines+markers', 
                                  line = list(width = 1),
                                  name = colnames(PHONE_COST_2017_8)[i])
}
p_dim2_Phone_2017 %>% add_trace(y=rowMeans(PHONE_COST_2017_8[,petits_indices_2]),x=c(8:16),
                                type="scatter",mode="lines+markers",line = list(width = 4),name="Moyenne des soumissions",color=I("#FC4E07"))%>%
  layout(title = "Petites coordonn�es sur Dim 2: Phone 2017") 


#Plot des 4 moyennes des d�penses pour chaque groupe de RTAs dans chaque dimension (plus grandes coordonn�es et plus petites)
plot_ly() %>% 
  add_trace(y=rowMeans(PHONE_COST_2017_8[,petits_indices_1]),x=c(8:16),type="scatter",mode="lines+markers",line = list(width = 4),name="Petites coordonn�es sur Dim 1: Phone 2017")%>%
  add_trace(y=rowMeans(PHONE_COST_2017_8[,grands_indices_1]),x=c(8:16),type="scatter",mode="lines+markers",line = list(width = 4),name="Grandes coordonn�es sur Dim 1: Phone 2017")%>%
  add_trace(y=rowMeans(PHONE_COST_2017_8[,grands_indices_2]),x=c(8:16),type="scatter",mode="lines+markers",line = list(width = 4),name="Grandes coordonn�es sur Dim 2: Phone 2017")%>%
  add_trace(y=rowMeans(PHONE_COST_2017_8[,petits_indices_2]),x=c(8:16),type="scatter",mode="lines+markers",line = list(width = 4),name="Petites coordonn�es sur Dim 2: Phone 2017")%>%
  layout(title = "Moyenne des d�penses en t�l�marketing dans les diff�rentes RTAs correspondants aux dimensions 1 et 2") 

#Plot des 4 moyennes des soumissions pour chaque groupe de RTAs dans chaque dimension (plus grandes coordonn�es et plus petites)
plot_ly() %>% 
  add_trace(y=rowMeans(SOUM_2017_8[,petits_indices_1]),x=c(8:16),type="scatter",mode="lines+markers",line = list(width = 4),name="Petites coordonn�es sur Dim 1: Phone 2017")%>%
  add_trace(y=rowMeans(SOUM_2017_8[,grands_indices_1]),x=c(8:16),type="scatter",mode="lines+markers",line = list(width = 4),name="Grandes coordonn�es sur Dim 1: Phone 2017")%>%
  add_trace(y=rowMeans(SOUM_2017_8[,grands_indices_2]),x=c(8:16),type="scatter",mode="lines+markers",line = list(width = 4),name="Grandes coordonn�es sur Dim 2: Phone 2017")%>%
  add_trace(y=rowMeans(SOUM_2017_8[,petits_indices_2]),x=c(8:16),type="scatter",mode="lines+markers",line = list(width = 4),name="Petites coordonn�es sur Dim 2: Phone 2017")%>%
  layout(title = "Moyenne des soumissions dans les diff�rentes RTAs correspondants aux dimensions 1 et 2") 


