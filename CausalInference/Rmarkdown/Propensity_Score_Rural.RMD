---
title: "MROI - Score de Propension / Milieu Rural"
author: "Chaimae Sriti"
output:
  html_document:
    highlight: tango
    number_sections: no
    theme: united
    toc: yes
    toc_depth: 4
  pdf_document:
    toc: yes
    toc_depth: '4'
  word_document: default
always_allow_html: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE ,warning=FALSE, error=FALSE, message=FALSE)
```


# Les donn�es 

Dans les donn�es ci-dessous, on a combin� les donn�es concernant les d�penses en t�l�marketing en volume par habitant, ainsi que les donn�es (covariables) provenant de StatCan.

- Il est � noter qu'on ne conserve que les 152 RTAs issues du plus gros cluster K-means (K=5).
- Pour faire l'ACP, on a supprim� les RTAs aberrantes (parmi les 152 RTAs), on a conserv� ainsi 128 RTAs.
- Pour calculer le score de propension, on prend, dans un premier lieu, les 30 RTAs o� l'on d�pense le plus (high), et les 30 RTAs o� l'on d�pense le moins (low).
- On rajoute la colonne (variable r�ponse) TREATED = 1 si RTA %in% high et = 0 si RTA %in% low.

```{r, echo=FALSE}
library(MatchIt)
library(dplyr)
library(plotly)
library(psych)
library(FactoMineR)
library(factoextra)
library(DT)
library(sf)
library(gridExtra)
library(leaflet)
library(data.table)
library(broom)
library(magrittr)
library(tidyverse)
library(ggplot2)

DATA<- read.table("C:\\Users\\chaimae\\Desktop\\Essai\\DATA.csv",sep=",",header=T)
RTA_SHP_QC <- readRDS("C:\\Users\\chaimae\\Desktop\\Essai\\rta_shp_qc.rds")
STATCAN <- read.table("C:\\Users\\chaimae\\Desktop\\Essai\\GEOBD_RTA.csv",sep=";",header=T)


DATA$URBAIN <- ifelse(grepl(0, DATA$GEO_RTA) == T, 0, 1)
STATCAN$URBAIN <- ifelse(grepl(0, STATCAN$GEO_RTA) == T, 0, 1)
STATCAN$GEO_NBF_2016 <- STATCAN$GEO_NBF_2016/1000

PHONE_DATA <- DATA %>% 
  # on filtre sur l'annee 2017, les semaines de 8 a 16
  # on retire les RTA sans population
  filter(GEO_NBF_2016> 0,YEAR == 2017, WEEKNUM %in% 8:16) %>% 
  # on cree le nombre de telemarketing par population
  mutate(
    PHONE__COST_POP = PHONE__COST / GEO_NBF_2016
  ) %>% 
  # avec la fonction spread, on obtient une ligne par RTA, et autant de colonnes que de semaines
  select(WEEKNUM, GEO_RTA, GEO_NBF_2016, PHONE__COST_POP) %>% 
  group_by(GEO_RTA,  GEO_NBF_2016) %>% 
  spread(WEEKNUM, PHONE__COST_POP) %>% 
  ungroup()

PHONE_DATA$MEAN <- rowMeans(PHONE_DATA[,3:11])
PHONE_DATA$SUMS <- rowSums(PHONE_DATA[,3:11])
PHONE_DATA$URBAIN <- ifelse(grepl(0, PHONE_DATA$GEO_RTA) == T, 0, 1)


STATCAN_2 <- STATCAN
STATCAN_2 <- merge(STATCAN_2,PHONE_DATA[,c(1,12)],by="GEO_RTA")

head(STATCAN_2[,1:7])
```

# Variables significatives pour la cr�ation des clusters 

Ici, on ajuste un mod�le GLM avec toutes les variables de StatCan, on fait une s�lection de variables "backwards", et on retient le mod�le final avec les variables ci-dessous.

```{r, results=FALSE}
formule <- paste("MEAN ~ ", 
                 paste(colnames(STATCAN_2[-c(1:3, 45:47)]), collapse = ' + ')) %>% 
  as.formula()

model.full <- glm(formula = formule, 
                  family = "gaussian", 
                  data = STATCAN_2)

backwards <-  step(model.full) 
```


```{r}
model_backwards <- glm(MEAN ~ P_POP_0024 + P_marie + P_commlaw + P_condo + P_unifam + 
                         P_movable + P_nooccupation + P_const_av1960 + P_mater_ang+
                         P_autoch + P_2564_collcegep + P_2564_uni_above + Ave_pers_menag + 
                         BURC + LARC + INCP_SCORE_STD + CRP3_SCORE_AVG,
                       family = "gaussian", 
                       data = STATCAN_2)
summary(model_backwards)
```

# Choix du nombre optimal de clusters

```{r, results=F}
vars_StatCan <- c("P_POP_0024","P_marie", "P_commlaw","P_condo","P_unifam", 
                  "P_movable", "P_nooccupation","P_const_av1960", "P_mater_ang", 
                  "P_autoch", "P_2564_collcegep","P_2564_uni_above", "Ave_pers_menag", 
                  "BURC","LARC","INCP_SCORE_STD","CRP3_SCORE_AVG")
library(NbClust)
# Index 
res <-NbClust(data =scale(STATCAN[,vars_StatCan]), distance = "euclidean",
       method = "kmeans")
```
 
* Among all indices:                                                
* 8 proposed 2 as the best number of clusters 
* 5 proposed 3 as the best number of clusters 
* 5 proposed 6 as the best number of clusters 
* 1 proposed 8 as the best number of clusters 
* 2 proposed 9 as the best number of clusters 
* 1 proposed 14 as the best number of clusters 
* 1 proposed 15 as the best number of clusters 

Ainsi, on d�cide de retenir 2 groupes.

# Classification K-Means (k=2) 

On classifie nos RTAs en deux groupes selon les variables de Statcan retenues. On utilise la classification K-Means :

```{r}
phone_kmean <- kmeans(
  x       = scale(STATCAN[,vars_StatCan]),
  centers = 2, 
  nstart  = 20
)
```

```{r, echo = F}
STATCAN$CLUSTER <- phone_kmean$cluster
STATCAN_Kmeans_2 <- STATCAN[STATCAN$CLUSTER==1,]
```

```{r}
phone_kmean$size
phone_kmean$withinss/phone_kmean$size    ## 285 133
```

Avant de faire l'ACP sur les RTAs retenus, on enl�ve les observations ab�rrantes :

```{r, echo = F}

PHONE_DATA_Kmeans_2 <- PHONE_DATA[STATCAN_Kmeans_2$GEO_RTA,]
no_outliers_pca <- PHONE_DATA_Kmeans_2 %>% 
  filter(`8`  < min(boxplot.stats(PHONE_DATA_Kmeans_2$`8`)$out) ) %>% 
  filter(`9`  < min(boxplot.stats(PHONE_DATA_Kmeans_2$`9`)$out) ) %>% 
  filter(`10` < min(boxplot.stats(PHONE_DATA_Kmeans_2$`10`)$out) ) %>% 
  filter(`11` < min(boxplot.stats(PHONE_DATA_Kmeans_2$`11`)$out) ) %>% 
  filter(`12` < min(boxplot.stats(PHONE_DATA_Kmeans_2$`12`)$out) ) %>% 
  filter(`13` < min(boxplot.stats(PHONE_DATA_Kmeans_2$`13`)$out) ) %>% 
  filter(`14` < min(boxplot.stats(PHONE_DATA_Kmeans_2$`14`)$out) ) %>% 
  filter(`15` < min(boxplot.stats(PHONE_DATA_Kmeans_2$`15`)$out) ) %>% 
  filter(`16` < min(boxplot.stats(PHONE_DATA_Kmeans_2$`16`)$out) )
no_outliers_pca <- as.data.frame(no_outliers_pca)
rownames(no_outliers_pca) <- no_outliers_pca$GEO_RTA

head(no_outliers_pca)

```

Combien on a de RTAs dans le groupe 1 ?

```{r}
nrow(no_outliers_pca)
```

# ACP {.tabset}

Les r�sultats de l'ACP r�alis�e sur les 247 RTAs du cluster 1, apr�s avoir supprim� les RTAs ab�rrantes.

```{r}
### pca on no_outliers_pca data
phone_pca <- PCA(
  X          = no_outliers_pca[,3:11],
  graph      = F,
  scale.unit = T
)
```

```{r, echo=F}
pca_eigval <- get_eigenvalue(phone_pca)
pca_vars <- get_pca_var(phone_pca)
pca_ind <- get_pca_ind(phone_pca)
```

## Proportion de variabilit� expliqu�e {-}

```{r}
fviz_eig(phone_pca, addlabels = TRUE)
```

## Graphique des semaines {-}

```{r}
fviz_pca_var(phone_pca, col.var = "cos2",
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),labelsize=4)
```

## Graphique des RTAs {-}

```{r}
fviz_pca_ind (phone_pca,  col.ind = "cos2",
              gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
              labelsize = 2)
```          
    
    
## High VS Low {-}

Visualisons les diff�rences entre les d�penses en t�l�marketing dans les deux groupes de RTAs qu'on a retenu ( taille = 50 dans chaque groupe ).

```{r, echo = F, width = 12}
phone_pca_coord <- as.data.frame(pca_ind$coord)

phone_pca_coord$GEO_RTA <- rownames(phone_pca_coord)
phone_pca_coord$URBAIN <- ifelse(grepl(0, phone_pca_coord$GEO_RTA) == T, 'RURAL', 'URBAIN')
phone_pca_coord <- arrange(phone_pca_coord,by=Dim.1)

phone_pca_low <- phone_pca_coord$GEO_RTA[5:54]
phone_pca_middle <-phone_pca_coord$GEO_RTA[c(1:4,55:197)]
phone_pca_high <- phone_pca_coord$GEO_RTA[198:247]

plot_ly() %>% 
  add_trace(y=as.numeric(colMeans(PHONE_DATA_Kmeans_2[PHONE_DATA_Kmeans_2$GEO_RTA %in% phone_pca_low,3:11])),x=c(8:16),type="scatter",mode="lines",line = list(width = 2),name="D�penses minimales / treated = 0")%>%
  add_trace(y=as.numeric(colMeans(PHONE_DATA_Kmeans_2[PHONE_DATA_Kmeans_2$GEO_RTA %in% phone_pca_high,3:11])),x=c(8:16),type="scatter",mode="lines",line = list(width = 2),name="D�penses maximales / treated = 1")%>%
  layout(title = "Moyenne des d�penses en t�l�marketing dans les diff�rentes RTAs")

```

## Carthographie {-}

```{r, echo = F}
data_Kmeans_temp <- PHONE_DATA_Kmeans_2 %>% left_join(
  STATCAN_Kmeans_2, by = c('GEO_RTA', 'GEO_NBF_2016',"URBAIN")
)

data_Kmeans_temp <- data_Kmeans_temp[data_Kmeans_temp$GEO_RTA %in% c(phone_pca_low,phone_pca_high),]
data_Kmeans_temp$treated <- ifelse(data_Kmeans_temp$GEO_RTA %in% phone_pca_low,0,1) 

DATA_PHONE <- PHONE_DATA %>% left_join(
  STATCAN, by = c('GEO_RTA','GEO_NBF_2016','URBAIN')
)

DATA_PHONE$GROUPS <- ifelse(DATA_PHONE$GEO_RTA %in% phone_pca_low, 'low', ifelse(DATA_PHONE$GEO_RTA %in% phone_pca_high,'high',ifelse(DATA_PHONE$GEO_RTA %in% phone_pca_middle,'middle',"others")))

DATA_PHONE_lf <- RTA_SHP_QC %>% 
  left_join(DATA_PHONE, by = 'GEO_RTA') %>% 
  mutate(
    popup = paste(GEO_RTA, 
                  '<br /> #Pop: ', GEO_NBF_2016, 
                  '<br /> #Phone/hab: ', round(MEAN, 2),
                  '<br /> Clust: ', CLUSTER,
                  '<br /> Group: ', GROUPS)
  )

# creation palette de couleurs
MaPalette0 <- colorNumeric(c('blue', 'white', 'red'), range(1e3 * DATA_PHONE_lf$MEAN))
MaPalette1 <- colorFactor(c('grey', 'black'), 0:1)
MaPalette2 <- colorFactor(c('#088A08', '#F4FA58', '#FF8000', '#0000FF', '#FF0000'), 
                          phone_kmean$cluster %>% factor %>% summary %>% sort(decreasing = T) %>% names())
#MaPalette21 <- colorFactor(c('#088A08'), 1)
#MaPalette22 <- colorFactor(c('#FFFF00'), 2)
#MaPalette23 <- colorFactor(c('#FF8000'), 3)
#MaPalette24 <- colorFactor(c('#61210B'), 4)
#MaPalette25 <- colorFactor(c('#DF01D7'), 5)
MaPalette3 <- colorFactor(c('black', 'white'), c('high', 'low'))

# creation de la map
leaflet(DATA_PHONE_lf) %>%  
  addProviderTiles(providers$OpenStreetMap) %>% 
  
  addPolygons(
    highlight   = highlightOptions(color = 'black'),
    fillColor   = MaPalette0(1e3 * DATA_PHONE_lf$MEAN),  
    fillOpacity = .6,
    weight      = 1.5, 
    color       = 'darkgrey',
    popup       = ~ popup,
    group       = 'Phone/mhab'
  ) %>% 
  addLegend('bottomright', pal = MaPalette0, values = 0:max(1e3 * DATA_PHONE$MEAN), 
            title = 'Phone/mhab', group = 'Phone/mhab') %>% 
  
  addPolygons(
    highlight   = highlightOptions(color = 'black'),
    fillColor   = MaPalette1(DATA_PHONE_lf$OUTLIERS),  
    fillOpacity = .6,
    weight      = 1.5, 
    color       = 'darkgrey',
    popup       = ~ popup,
    group       = 'Out'
  ) %>% 
  addLegend('bottomleft', pal = MaPalette1, values = 0:1, 
            title = 'Out', group = 'Out') %>% 
  
  #addPolygons(weight = 1.5, fillOpacity = 0, color = MaPalette21(), group = 'Clust1') %>% 
  #addPolygons(weight = 1.5, fillOpacity = 0, color = MaPalette22(), group = 'Clust2') %>% 
  #addPolygons(weight = 1.5, fillOpacity = 0, color = MaPalette23(), group = 'Clust3') %>% 
  #addPolygons(weight = 1.5, fillOpacity = 0, color = MaPalette24(), group = 'Clust4') %>% 
  #addPolygons(weight = 1.5, fillOpacity = 0, color = MaPalette25(), group = 'Clust5') %>% 
  addPolygons(
    highlight   = highlightOptions(color = 'black'),
    fillColor   = MaPalette2(DATA_PHONE_lf$CLUSTER),  
    fillOpacity = .8,
    weight      = 1.5, 
    color       = 'darkgrey',
    popup       = ~ popup,
    group       = 'Clusts'
  ) %>% 
  addLegend('bottomleft', pal = MaPalette2, values = 1:2, title = 'Clusts', group = 'Clusts') %>% 
  
  addPolygons(
    highlight   = highlightOptions(color = 'black'),
    fillColor   = MaPalette3(DATA_PHONE_lf$GROUPS),  
    fillOpacity = ifelse(DATA_PHONE_lf$GROUPS %in% c('high', 'low'), .6, 0),  
    weight      = 1.5, 
    color       = 'darkgrey',
    popup       = ~ popup,
    group       = 'High/low'
  ) %>% 
  addLegend('bottomleft', pal = MaPalette3, values = c('high', 'low'), 
            title = 'High/low', group = 'High/low') %>% 
  
  addLayersControl(
    baseGroups    = c('Clusts', 'Out', 'Phone/mhab'),
    overlayGroups = c('High/low'),
    options       = layersControlOptions(collapsed = F)
  )

```

## Comparaison

```{r, warning = F, echo = F}
vars_StatCana <- colnames(STATCAN)[4:44]

# est-ce que high et low ont les memes characteristiques ?
bind_rows(
  # moyenne par groupe
  DATA_PHONE %>%
    filter(GROUPS %in% c('high', 'low')) %>% 
    select(GROUPS, one_of(vars_StatCana)) %>%
    group_by(GROUPS) %>%
    summarise_all(funs(mean)),
  
  # sd par groupe
  DATA_PHONE %>%
    filter(GROUPS %in% c('high', 'low')) %>% 
    select(GROUPS, one_of(vars_StatCana)) %>%
    group_by(GROUPS) %>%
    summarise_all(funs(sd)),
  
  # ajout t.test
  DATA_PHONE %>% 
    filter(GROUPS %in% c('high', 'low')) %>% 
    select(GROUPS, one_of(vars_StatCana)) %>% 
    summarise_at(
      vars(-GROUPS),
      funs(t.test(.[GROUPS == "high"], .[GROUPS == "low"])$p.value %>% round(3))
  )
) %>% 
  mutate(GROUPS = c('mean_high', 'mean_low', 'sd_high', 'sd_low', 't.test')) %>% 
  gather(Variable, value, -GROUPS) %>% 
  spread(GROUPS, value) %>%
  
  datatable(options(dom = 'tp'), rownames = F) %>% 
  formatRound(2:5, digits = 2) %>%
  formatStyle('Variable', fontWeight = 'bold') %>% 
  formatStyle('t.test', color = styleInterval(c(.05, .1), c('red', 'orange', NA)))
```

# Diff�rence dans les moyennes: variable d�pendante 

On souhaite avoir une id�e sur la diff�rence dans les moyennes entre le groupe des RTAs trait�es et le groupe des RTAs non trait�es.


```{r, echo = F}
data_Kmeans_temp <- as.data.frame(data_Kmeans_temp)
data_Kmeans_temp_cov <- colnames(data_Kmeans_temp)[16:56]
data_Kmeans_temp %>%
  group_by(treated) %>% 
  summarise(n_rtas = n(),
            mean_phone_volume = mean(MEAN),
            std_error = sd(MEAN) / sqrt(n_rtas))
```

La diff�rence entre les deux moyennes est statistiquement significative au seuil de 5%:

```{r, echo = F}
with(data_Kmeans_temp, t.test(MEAN ~ treated))  
```


## Diff�rence dans les moyennes: covariables de pr�traitement (donn�es StatCan)

```{r, echo = F}
data_Kmeans_temp <- as.data.frame(data_Kmeans_temp)
data_Kmeans_temp_cov <- colnames(data_Kmeans_temp)[16:56]

means_covariates <- data_Kmeans_temp %>%
  group_by(treated) %>%
  select(one_of(data_Kmeans_temp_cov)) %>%
  summarise_all(funs(mean(., na.rm = T)))
```

Nous pouvons effectuer des tests t pour �valuer si ces moyennes sont statistiquement diff�rentes:


```{r, echo = F}
list <- lapply(data_Kmeans_temp_cov, function(v){
  t.test(data_Kmeans_temp[,v] ~ data_Kmeans_temp[,'treated'])$p.value
})

p_val <- unlist(list)
p_value <- data.frame(data_Kmeans_temp_cov,p_val)
p_value$sign <- ifelse(p_value$p_val< 0.05,'diff','equal')
```


```{r}
head(p_value)
```

Les variables dont les moyennes sont significativement diff�rentes du groupe Treated = 1 au groupe Treated = 0 sont :

```{r}
data_Kmeans_temp_cov_diff <- p_value$data_Kmeans_temp_cov[p_value$sign=='diff']
data_Kmeans_temp_cov_diff
```

Les variables dont les moyennes ne sont pas significativement diff�rentes du groupe Treated = 1 au groupe Treated = 0 sont :

```{r}
data_Kmeans_temp_cov_equ <- p_value$data_Kmeans_temp_cov[p_value$sign=='equal']
data_Kmeans_temp_cov_equ
```

# R�capitulatif {.tabset}

## Mod�le 1  {.tabset}

### Treated/Covariates_StatCan

```{r}
DATA_TMLE <- read.table("C:\\Users\\chaimae\\Documents\\DATA_All_Covariates.csv",sep=",",header=T)
summary(DATA_TMLE$SOUM_MEANS)
boxplot(DATA_TMLE$SOUM_MEANS~DATA_TMLE$treated)
```

```{r}
m_ps <-  glm(treated ~ P_marie +
               P_2564_collcegep + P_2564_uni_above +CRP3_SCORE_AVG,
             family = binomial(link = "logit"), data = DATA_TMLE)
summary(m_ps)

library(pROC)
g <- roc(treated ~ predict(m_ps,type=c("response")), data = DATA_TMLE)
g
```

### Propensity Score

```{r}
prs_df_matching <- data.frame(pr_score = round(predict(m_ps, type = "response"),10),
                              treated = m_ps$model$treated)
D <- pmin(1-prs_df_matching$pr_score,prs_df_matching$pr_score)
Q <- prs_df_matching$treated*prs_df_matching$pr_score + (1 -prs_df_matching$treated)*(1-prs_df_matching$pr_score)
prs_df_matching$matching_weight_1 <- D/Q
prs_df_matching$matching_weight_2 <- ifelse(prs_df_matching$treated == 1,1/prs_df_matching$pr_score,1/(1-prs_df_matching$pr_score))
round(prs_df_matching,10)
```

### logit(Propensity_Score)

```{r}
m_soum <- lm(SOUM_MEANS ~ treated + logit(prs_df_matching$pr_score),
             data = DATA_TMLE)

summary(m_soum)
```

### Treated

```{r}
m_soum_ps <- lm(SOUM_MEANS ~ treated ,
             data = DATA_TMLE)
summary(DATA_TMLE$SOUM_MEANS)
boxplot(DATA_TMLE$SOUM_MEANS~DATA_TMLE$treated)
summary(m_soum_ps)

```


###  Matching Weights 1

```{r}
m_soum_weights_1 <- lm(SOUM_MEANS ~ treated , weights = prs_df_matching$matching_weight_1,
                       data = DATA_TMLE)

summary(m_soum_weights_1)

library(sandwich)
vcovHC(m_soum_weights_1, type = "HC")
sandwich_se <- diag(vcovHC(m_soum_weights_1, type = "HC"))^0.5
sandwich_se
coef(m_soum_weights_1)-1.96*sandwich_se
coef(m_soum_weights_1)+1.96*sandwich_se
```

###  Matching Weights 2

```{r}
m_soum_weights_2 <- lm(SOUM_MEANS ~ treated  , weights = prs_df_matching$matching_weight_2,
                       data = DATA_TMLE)

summary(m_soum_weights_2)

library(sandwich)
vcovHC(m_soum_weights_2, type = "HC")
sandwich_se <- diag(vcovHC(m_soum_weights_2, type = "HC"))^0.5
sandwich_se
coef(m_soum_weights_2)-1.96*sandwich_se
coef(m_soum_weights_2)+1.96*sandwich_se

```

### Mod�le �labor�

```{r}
m_soum_weights_2 <- lm(SOUM_MEANS ~ treated + MAILA__COST+RADIO__COST+TELE__COST+P_mater_fr+P_POP_65o+Ave_pers_menag,
                           data = DATA_TMLE)

summary(m_soum_weights_2)
```

