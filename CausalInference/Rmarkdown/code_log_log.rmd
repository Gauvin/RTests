---
title: "Mod�le de r�gression - Sp�cification Log-Log"
author: "Chaimae Sriti"
date: "21 D�cembre 2018"
output:
  html_document:
    highlight: tango
    number_sections: no
    theme: united
    toc: yes
    toc_depth: 4
  pdf_document:
    toc: yes
    toc_depth: '4'
  word_document: default
always_allow_html: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE ,warning=FALSE, error=FALSE, message=FALSE)
```


# Mod�le 1 - Les donn�es hebdomadaires

Dans ce premier mod�le, on explique les d�penses en t�l�marketing et les soumissions par les variables GEO_POP et GEO_NBF, on travaille sur les donn�es pour les semaines 8 � 16 de l'ann�e 2017.

```{r, echo=FALSE}
DATA<- read.table("C:\\Users\\chaimae\\Desktop\\Essai\\mroi_data_quebec_CCC.csv",sep=";",header=T)
StatCan_Data <- read.table("C:\\Users\\chaimae\\Desktop\\Essai\\GEOBD_RTA.csv",sep=";",header=T)
indices<- read.table( "C:\\Users\\chaimae\\Documents\\indices.txt",sep=",",header=T)
library(dplyr)
library(tidyr)
Data_8weeks_2017 <- DATA %>% 
  # on filtre sur l'annee 2017, les semaines de 8 a 16
  # on retire les RTA sans population
  filter(GEO_NBF_2016>0,YEAR == 2017,WEEKNUM %in% 8:16,PHONE__COST>0,GEO_RTA %in% indices$RTA
         # ,SOUM>0
         )%>%  select(GEO_RTA,WEEKNUM,GEO_NBF_2016,GEO_POP,WEEKNUM,PHONE__COST,SOUM)%>% 
  # on cree le nombre de telemarketing par population
  ungroup()

StatCan_D <- StatCan_Data %>% 
  filter(GEO_NBF_2016>0
  )%>%  select(GEO_RTA,C16POPDEN)%>% 
  ungroup()


Data_8weeks_2017 <- merge(Data_8weeks_2017,StatCan_D,by="GEO_RTA")  
head(Data_8weeks_2017)
```

## Variable d�pendante : PHONE__COST

Dans un premier temps, on ajuste le mod�le avec la variable ind�pendante log(GEO_NBF_2016), ensuite, avec la variable log(GEO_POP)

### Variable ind�pendante : GEO_NBF

```{r}
log_log_Phone_NBF <- lm(log(PHONE__COST) ~ log(GEO_NBF_2016), data=Data_8weeks_2017)
```


```{r}
summary(log_log_Phone_NBF)   #b1 = 0.92   R^2 = 0.32
```

### Variable ind�pendante : GEO_POP

```{r}
log_log_Phone_POP <- lm(log(PHONE__COST) ~ log(GEO_POP), data=Data_8weeks_2017)
```


```{r}
summary(log_log_Phone_POP)   #b1 = 0.73 R^2 = 0.19
```


## Variable d�pendante : SOUM

Dans un premier temps, on ajuste le mod�le avec la variable ind�pendante log(GEO_NBF_2016), ensuite, avec la variable log(GEO_POP)

```{r, echo=FALSE}
Data_8weeks_2017 <- DATA %>% 
  # on filtre sur l'annee 2017, les semaines de 8 a 16
  # on retire les RTA sans population
  filter(GEO_NBF_2016>0,YEAR == 2017,WEEKNUM %in% 8:16,SOUM>0
         # ,SOUM>0
         )%>%  select(GEO_RTA,WEEKNUM,GEO_NBF_2016,GEO_POP,WEEKNUM,PHONE__COST,SOUM)%>% 
  # on cree le nombre de telemarketing par population
  ungroup()

head(Data_8weeks_2017)
```

### Variable ind�pendante : GEO_NBF

```{r}
log_log_SOUM_NBF <- lm(log(SOUM) ~ log(GEO_NBF_2016), data=Data_8weeks_2017)
```


```{r}
summary(log_log_SOUM_NBF)   #b1 = 0.88051   R^2 = 0.55
```

### Variable ind�pendante : GEO_POP


```{r}
log_log_SOUM_POP <- lm(log(SOUM) ~ log(GEO_POP), data=Data_8weeks_2017)
```


```{r}
summary(log_log_SOUM_POP)   #b1 = 0.79061   R^2 = 0.40
```


# Mod�le 2 - La moyenne des donn�es hebdomadaires (une donn�e par RTA)

Dans ce mod�le, on utilise la moyenne des d�penses en t�l�marketing et des soumissions sur les 9 semaines, ainsi, on aura une donn�e par RTA.


```{r, echo=FALSE}
indices<- read.table( "C:\\Users\\chaimae\\Documents\\indices.txt",sep=",",header=T)
Data_8weeks_group_2017 <- DATA %>% 
  # on filtre sur l'annee 2017, les semaines de 8 a 16
  # on retire les RTA sans population
  filter(GEO_NBF_2016>0,YEAR == 2017,WEEKNUM %in% 8:16 ,PHONE__COST>0, GEO_RTA %in% indices$RTA) %>% 
  select(GEO_RTA,GEO_NBF_2016,GEO_POP,WEEKNUM,PHONE__COST,SOUM)%>% 
  # on cree le nombre de telemarketing par population  
  group_by(GEO_RTA,GEO_NBF_2016,GEO_POP) %>%
  summarise_at(vars(-c(1:4)), funs(mean(., na.rm=TRUE)))%>%
  ungroup()


StatCan_Data <- read.table("C:\\Users\\chaimae\\Desktop\\Essai\\GEOBD_RTA.csv",sep=";",header=T)

StatCan_D <- StatCan_Data %>% 
  filter(GEO_NBF_2016>0
  )%>%  select(GEO_RTA,C16POPDEN)%>% 
  ungroup()


Data_8weeks_group_2017 <- merge(Data_8weeks_group_2017,StatCan_D,by="GEO_RTA")  
head(Data_8weeks_group_2017)
View(Data_8weeks_group_2017)
```

## Variable d�pendante : PHONE__COST

On ajuste le mod�le avec la variable ind�pendante log(GEO_NBF_2016).


```{r}
model_log_PHONE_NBF <- lm(log(PHONE__COST) ~ log(GEO_NBF_2016), data=Data_8weeks_group_2017)
```


```{r}
summary(model_log_PHONE_NBF)   # b1 = 0.95026   R^2 = 0.49
```


On ajuste le mod�le avec la variable ind�pendante log(GEO_POP).


```{r}
model_log_PHONE_POP <- lm(log(PHONE__COST) ~ log(GEO_POP), data=Data_8weeks_group_2017)
```


```{r}
summary(model_log_PHONE_POP)   #b1 = 0.78928   R^2 = 0.35
```

On ajuste le mod�le avec la variable ind�pendante log(C16POPDEN).


```{r}
model_log_PHONE_DEN <- lm(log(PHONE__COST) ~ log(C16POPDEN), data=Data_8weeks_group_2017)
```


```{r}
summary(model_log_PHONE_DEN)   #b1 =-0.14503  R^2 = 0.05
```

## Analyse des r�sidus

Examinons les r�sidus issus du mod�le log-log avec la variable d�pendante : moyenne des d�penses en t�l�marketing par semaine, et la variable d�pendante GEO_NBF.

```{r, echo=FALSE}
residuals_log_log_NBF_mean <-data.frame(Data_8weeks_group_2017$GEO_RTA,model_log_PHONE_NBF$residuals,model_log_PHONE_POP$residuals,model_log_PHONE_DEN$residuals)
colnames(residuals_log_log_NBF_mean) <- c("RTA","Residuals_NBF","Residuals_POP","Residuals_POPDEN")
```

```{r, echo=FALSE}
rownames(indices) <- indices$RTA
indices$X <- NULL
indices_residuals <- merge(indices,residuals_log_log_NBF_mean,by='RTA')
head(indices_residuals)
```


### ACP - graphique avec la couleur de la RTA selon la dimension 1

```{r, echo=FALSE}
library(plotly)

plot_ly(
  data  = indices, 
  x     = ~ Dim.1, 
  y     = ~ Dim.2,
  color = ~ Dim.1, 
  text = ~ RTA,
   colors = c("yellow","blue"),
  type= 'scatter',
  mode='markers'
)
```


### ACP - graphique avec la couleur de la RTA selon les R�sidus - GEO_POP

```{r, echo=FALSE}
plot_ly(
  data  = indices_residuals, 
  x     = ~ Dim.1, 
  y     = ~ Dim.2,
  color = ~ Residuals_POP, 
  text = ~ RTA,
   colors = c("yellow","blue"),
  type= 'scatter',
  mode='markers'
)
```



### ACP - graphique avec la couleur de la RTA selon les R�sidus - GEO_NBF

```{r, echo=FALSE}
plot_ly(
  data  = indices_residuals, 
  x     = ~ Dim.1, 
  y     = ~ Dim.2,
  color = ~ Residuals_NBF, 
  text = ~ RTA,
   colors = c("yellow","blue"),
  type= 'scatter',
  mode='markers'
)
```



### ACP - graphique avec la couleur de la RTA selon les R�sidus - GEO_POP_DEN

```{r, echo=FALSE}
plot_ly(
  data  = indices_residuals, 
  x     = ~ Dim.1, 
  y     = ~ Dim.2,
  color = ~ Residuals_POPDEN, 
  text = ~ RTA,
   colors = c("yellow","blue"),
  type= 'scatter',
  mode='markers'
)
```



