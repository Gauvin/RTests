---
title: "testH"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

library(sf)

library(magrittr)
library(tidyverse)
library(ggplot2)

library(glue)
library(here) 

library(ceramic)
library(slippymath)
library(raster)

library(curl)

```
# Set the api key
```{r}

dfApi <- read_delim( file = here("apiKeys"), 
                     delim="=",
                     col_names = F)

Sys.setenv(MAPBOX_API_KEY = dfApi$X2 )
```

# Params
```{r}

zz <- 19

```
# Point to query
```{r}


point <- c(-71.236159 , 46.804292) 
point2 <- c(-71.221745, 46.811633 )

shpPoint <- st_point(point) %>% st_sfc %>% st_sf
shpPoint2 <- st_point(point2) %>% st_sfc %>% st_sf

```

# Buffers
```{r}

dfBboxInfo1 <- slippymath::bbox_tile_query(shpPoint %>% st_bbox())

```

```{r}

point_grid <- bbox_to_tile_grid(shpPoint %>% st_bbox(),zoom = zz)

```

# Curl download the jpg for all tiles
```{r}

curlDownload <- function(x, y, zoom){
  
  mapbox_query_string <-
    paste0("https://api.mapbox.com/v4/mapbox.terrain-rgb/{zoom}/{x}/{y}.jpg90",
           "?access_token=",
           Sys.getenv("MAPBOX_API_KEY")
           )
  
  outfile <- glue("{x}_{y}.jpg")
  GeneralHelpers::createDirIfNotExistentRecursive(here("Data", "jpg", zoom))
  curl::curl_download(url = glue(mapbox_query_string),
                destfile = here("Data", "jpg", zoom, outfile))
  return(outfile)
  
  
}

```

```{r}

# 
# 
# qc_tiles <-
# pmap(.l = point_grid$tiles,
#      zoom = point_grid$zoom,
#      .f = curlDownload
#      )
qc_tiles <- 

```


# Stich to raster
```{r}

qc_raster <- slippymath::compose_tile_grid(point_grid,
                                           here("Data", "jpg", zz ,  qc_tiles[[1]] ) )

```