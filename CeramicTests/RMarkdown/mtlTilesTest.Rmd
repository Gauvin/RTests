---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

 
#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

library(sf)

library(magrittr)
library(tidyverse)
library(ggplot2)

library(glue)
library(here) 

library(ceramic)
library(raster)

library(gdalUtils)

```

# Params
```{r}

proj32198 <- "+proj=lcc +lat_1=60 +lat_2=46 +lat_0=44 +lon_0=-68.5 +x_0=0 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs"

proj4326 <- "+proj=longlat +datum=WGS84 +no_defs"

projMerc <- '+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext +no_defs'


```

# Set the api key
```{r}

dfApi <- read_delim( file = here("apiKeys","apiKeysMapbox"), 
                     delim="=",
                     col_names = F)

Sys.setenv(MAPBOX_API_KEY = dfApi$X2 )
```

# Census
```{r}

Census2SfSp:::setCancensusOptions()

```


## Read stats can data
```{r}

Census2SfSp:::setCancensusOptions() 

listVectors <- c("v_CA16_2213", #median income
                 "v_CA16_2540", #prevalence of low income
                 "v_CA16_406")  #pop denisty per sq km

shpCensusMtl <- cancensus::get_census("CA16",
                      level = "DA",
                      regions = list(CSD="2466023"),
                      vectors = listVectors,
                      labels ="short",
                      geo_format = "sf"
                        ) %>% 
  st_transform(crs=proj32198)

shpCensusMtl %>% head
 

shpCensusMtl %>% dim



```

#Get mtl tiles around mont-royal station 
```{r}



#Watch out many 
mtlSatImg <- cc_location(loc=c(-73.582000 , 45.524944),
            buffer= c(10**3, 10**3),
            zoom = 19 ,
            type = "mapbox.satellite")


fileMontRoyalRaw <- here("Data","Raster", 'montRoyal' , 'montRoyalMercator.tif')
writeRaster(x=mtlSatImg, 
            filename=fileMontRoyalRaw )


plotRGB(mtlSatImg)
```

#Write the bbox as a shp file
```{r}

shpBbox <- (mtlSatImg) %>% SfSpHelpers:::bbox_polygon() %>% st_sfc %>% st_sf(crs=st_crs(mtlSatImg))
st_write(obj = shpBbox,
         dsn=here("Data","Shp","shpBboxMtlRoyalMercator.shp"))

```

#Reproject
```{r}

 
fileOut <- here("Data","Raster", 'montRoyal' , "montRoyal32198.tif")
fileOut4326 <- here("Data","Raster", 'montRoyal' , "montRoyal4326.tif")

#Reproject
if(!file.exists(fileOut)){
     gdalwarp(srcfile = fileMontRoyalRaw,
          dstfile = fileOut,
          t_srs = proj32198,
          overwrite = T
          )
}

if(!file.exists(fileOut4326)){
     gdalwarp(srcfile = fileMontRoyalRaw,
          dstfile = fileOut4326,
          t_srs = proj4326,
          overwrite = T
          )
} 
 

```
 
#Get mtl tiles around rox
```{r}

shpPointRox <- st_point( c(-73.808406, 45.505176) ) %>% 
  st_sfc %>% 
  st_sf(crs=proj4326) %>% 
  st_transform(crs=st_crs(shpCensusMtl) )
 
idxDa <- map_int(st_within(shpPointRox, shpCensusMtl  ), 1) 


shpGrid <- st_make_grid(shpCensusMtl[idxDa, ], cellsize = 50) 

shpCentroids <- shpGrid %>% 
  st_centroid() %>% 
  st_transform(crs=proj4326) %>% 
  SfSpHelpers::getCentroids()
  
dfCentroids <- shpCentroids %>% 
  dplyr::select(lng,lat)

#Watch out many 
mtlSatImg <- cc_location(loc=dfCentroids,
            buffer= c(10**3, 10**3),
            zoom = 19 ,
            type = "mapbox.satellite")


fileRox <- here("Data","Raster", 'roxboro' , 'roxboroMercator.tif')
writeRaster(x=mtlSatImg, 
            filename=fileRox )


plotRGB(mtlSatImg)


```


#Write the bbox as a shp file
```{r}

shpBbox <- (mtlSatImg) %>% 
  SfSpHelpers:::bbox_polygon() %>%
  st_sfc %>% 
  st_sf(crs=st_crs(mtlSatImg))

st_write(obj = shpBbox,
         dsn=here("Data","Shp","roxboro", "shpBboxRoxMercator.shp"),
          delete_layer=T)

```

#Reproject
```{r}

 
fileOut <- here("Data","Raster", 'roxboro' , "roxboro32198.tif")
fileOut4326 <- here("Data","Raster", 'roxboro' , "roxboro4326.tif")

#Reproject
if(!file.exists(fileOut)){
     gdalwarp(srcfile = fileRox,
          dstfile = fileOut,
          t_srs = proj32198,
          overwrite = T
          )
}

if(!file.exists(fileOut4326)){
     gdalwarp(srcfile = fileRox,
          dstfile = fileOut4326,
          t_srs = proj4326,
          overwrite = T
          )
} 
 

```
 
---
---


# Check existing tiles
```{r}

dfExist <- ceramic::ceramic_tiles(zoom=19)

```

## Filter according to extent  
```{r}

checkWithinExtent <- function(xmin,
                              xmax,
                              ymin,
                              ymax,
                              extent){
  
      withinBbox <- ifelse(xmin >= extent$xmin & xmax <=extent$xmax & ymin >= extent$ymin & ymax <= extent$ymax,
      T,
      F)
      
        return(withinBbox)
      }
  
                              

```

```{r}

idxKeep <- checkWithinExtent(dfExist$xmin,
                              dfExist$xmax,
                              dfExist$ymin,
                              dfExist$ymax,
                            st_bbox(mtlSatImg))

dfExistFiltered <- dfExist[idxKeep, ]

```
 
 
## Manually read the existing jpg and convert to raster
```{r}

manualReadWriteRaster <- function(rowDat){
  
  raster_img <- NULL
  tryCatch({
    
    im <- as.character(rowDat$fullname)
    rowXY <- rowDat %>% dplyr::select(  xmin  ,  xmax ,  ymin ,  ymax)
    x <- rowDat %>% dplyr::pull(tile_x)
    y <- rowDat %>% dplyr::pull(tile_y)
    zoom <- rowDat%>% dplyr::pull(zoom)
    
    raster_img <- raster::brick(im,
                                crs = projMerc)
    
 
    raster::extent(raster_img) <- raster::extent(
      matrix(c(rowXY$xmin , rowXY$ymin,
               rowXY$xmax, rowXY$ymax),nrow=2)
      )

    
    listPath <- (im %>% strsplit(split = "/"))[[1]]
    subParts <- paste( listPath[(length(listPath)-2):(length(listPath)-1)] , collapse="/" )
    lastPart <- strsplit(listPath[length(listPath)], ".jpg")[[1]]
    
    GeneralHelpers::createDirIfNotExistentRecursive(here("Data","Raster", subParts))
    fileOut <- here("Data","Raster", subParts, glue("{lastPart}.tiff") )
    if (!file.exists(fileOut)){
      writeRaster( raster_img , filename = fileOut)
    }
    
  },error=function(e){
    print(glue("Fatal error with x:{x} - y:{y} - {lastPart} - error:  {e}"))
  })
 
  
  return(raster_img) 
}

```

```{r}

manualReadWriteRaster(dfExistFiltered[1, ])

```

```{r}

getRasterTileMerged <- function(dfExistFiltered, numRast){
  
  browser()
  
  listRastBricks <- map( 1:(min(numRast,nrow(dfExistFiltered))),
                       ~manualReadWriteRaster(dfExistFiltered[.x, ] 
                       )
  )
  
  listRastBricks <- listRastBricks[ map_lgl(listRastBricks, ~!is.null(.x))]
   
  #Merge returns a single raster layer with a single raster birck,whih is not waht we want
  if(length(listRastBricks)==1){
    rasterMerged <- listRastBricks[[1]]
  }else{
     rasterMerged <- do.call(raster::merge, listRastBricks)
  }
 
  
  rasterMerged <- raster::projectRaster(rasterMerged,crs=proj32198)
  
  return(rasterMerged)
}



```

```{r}

r1 <- getRasterTileMerged(dfExistFiltered, numRast = 1)
r10 <- getRasterTileMerged(dfExistFiltered, numRast = 10)


plotRGB(r1)
plotRGB(r10)

```

## Crop the rasters using the census shp file to see how many polygons fall within the raster extent

```{r}


ext1 <- extent(r1)
ext <- extent(r10)

box <- c(xmin= attr( ext , "xmin"),
        xmax= attr( ext , "xmax"),
        ymin = attr( ext , "ymin"),
        ymax = attr( ext , "ymax")
)

shpBboxr10 <- SfSpHelpers::bbox_polygon(box) %>% 
  st_sfc %>% 
  st_sf(crs=crs(r10))


shpCensusMtlTouches <- shpCensusMtl[ map_lgl( st_intersects(shpCensusMtl, shpBboxr10), ~ !is_empty(.x)), ]

shpGrid <- st_make_grid(shpCensusMtlTouches, cellsize = 100)

ggplot() + 
  geom_sf(data=shpCensusMtlTouches)+
  geom_sf(data=shpGrid,col='green', alpha=0.2) + 
  geom_sf(data=shpBboxr10, col="red", alpha=0.2) 



```

## Finer grid 

```{r}

sizeRasterTile <- min( attr( ext1 , "xmax")-attr( ext1 , "xmin"),
                        attr( ext1 , "ymax")-attr( ext1 , "ymin"))

shpGridFiner <- st_make_grid(shpCensusMtlTouches, 
                        cellsize = sizeRasterTile)

ggplot() + 
  geom_sf(data=shpCensusMtlTouches)+
  geom_sf(data=shpGridFiner,col='green', alpha=0.2) + 
  geom_sf(data=shpBboxr10, col="red", alpha=0.2) 

```

```{r}

intersectSafe <- function(r,s){
  
  ex <- as_Spatial(s)
  cc <- NULL
  tryCatch({
    cc <- raster::intersect( r10 , ex)
  },
  error=function(e){
    print(glue("Fatal error - {e}"))
  })
  return(cc)
}

shpGridFinerBis <- st_sf(shpGridFiner, crs=crs(r10))  
listRastersInter <- map(1:nrow(shpGridFinerBis),
    ~intersectSafe( r10, shpGridFinerBis[.x, ])
)

idxNotNull <- map_lgl(listRastersInter , ~!is.null(.x))

listRastersInter <- listRastersInter[idxNotNull ]                
listRastersInter

raster::crop( r10 , as_Spatial(shpGridFinerBis)) %>% plotRGB()
raster::intersect( r10 , as_Spatial(shpGridFinerBis)) %>% plotRGB()


listRastersInter[[1]] %>% plotRGB()
listRastersInter[[2]] %>% plotRGB()
listRastersInter[[3]] %>% plotRGB()
```
